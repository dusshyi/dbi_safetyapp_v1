﻿DROP TABLE EmailLog;

CREATE TABLE EmailLog (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	EmailId INT NOT NULL,
	ReceivedBy VARCHAR(256) NOT NULL,
	SentBy VARCHAR(256) NOT NULL DEFAULT SYSTEM_USER,
	CreatedDate DATETIMEOFFSET NOT NULL DEFAULT GETDATE(),	
	[Timestamp] DATETIMEOFFSET NOT NULL DEFAULT GETDATE()
);