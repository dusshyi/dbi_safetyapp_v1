﻿DROP TABLE Category;

CREATE TABLE Category (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	FormId INT NOT NULL,
	[Name] VARCHAR(MAX) NOT NULL,
	[Description] VARCHAR(MAX) NULL,
	IsActive BIT NOT NULL DEFAULT 1,
	SortOrderId INT NOT NULL,
	CreatedBy VARCHAR(256) NOT NULL DEFAULT SYSTEM_USER,
	CreatedDate DATETIMEOFFSET NOT NULL DEFAULT GETDATE(),	
	[Timestamp] DATETIMEOFFSET NOT NULL DEFAULT GETDATE()
);

INSERT INTO Category (FormId, [Name], [Description], IsActive, SortOrderId)
VALUES 
(1, 'People', 'Question', 1, 1),
(1, 'Work in Traffic/On Rail', 'Question', 1, 2),
(1, 'Tools & Equipment', 'Question', 1, 3),
(1, 'Procedures', 'Question', 1, 4),
(1, 'Driving', 'Question', 1, 5),
(2, 'Environmental Hazards', 'Hazards', 1, 1),
(2, 'Ergonomic Hazards', 'Hazards', 1, 2),
(2, 'Access-Egress/ Operational Hazards', 'Hazards', 1, 3),
(2, 'Severity', 'Severity', 1, 1),
(2, 'Probability', 'Probability', 1, 1),
(1, 'Observer Title', 'Observer Title', 1, 1),
(1, 'Answer', 'Answer', 1, 1),
(1, 'Work Zone Intrusions', 'Sub question', 1, 1)