﻿DROP TABLE Email;

CREATE TABLE Email (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	FormId INT NOT NULL,
	[Subject] VARCHAR(256) NOT NULL,
	Body VARCHAR(MAX) NOT NULL,
	[Description] VARCHAR(MAX) NULL,
	IsActive BIT NOT NULL DEFAULT 1,
	CreatedBy VARCHAR(256) NOT NULL DEFAULT SYSTEM_USER,
	CreatedDate DATETIMEOFFSET NOT NULL DEFAULT GETDATE(),	
	[Timestamp] DATETIMEOFFSET NOT NULL DEFAULT GETDATE()
);

INSERT INTO Email (FormId, [Subject], Body, [Description], IsActive)
VALUES 
(1, 'Work zone intrusions - At Risk', '<!DOCTYPE html>
                                                            <html>
                                                                <head>
                                                                    <meta charset=''utf - 8'' />
                                                                    <title> Work Zone Intrusion - At Risk </title>
                                                                </head>
                                                                <body>
                                                                    <h3 style=''margin-bottom:-10px;''>SOC NOTIFICATION</h3>
                                                                    <h4>Work zone inrusion - At Risk</h4>
                                                                    <a href=''https://www.google.com/maps/search/?api=1&query={0},{1}''>
                                                                    <img border=''0'' 
                                                                        src=''https://maps.googleapis.com/maps/api/staticmap?center={0},{1}&zoom=18&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:*%7C{0},{1}&key=AIzaSyAYrQoDLURs6EmTxSQ4VAtlNjJy9QhwBwA'' 
                                                                        alt=''Work Zone Intrusion - At Risk location''></a> </br></br>
                                                                    <span><strong>Location:</strong> {2}</span></br>
                                                                    <span><strong>Date/Time observed:</strong> {3}</br>
                                                                    <span><a href=''{4}''>View safety observation card</a></span>
                                                                </body>
                                                            </html>', 'SOC Notification', 1),
(1, 'Hazards Identified (FLHA)/Risk Understood - At Risk', '<!DOCTYPE html>
                                                            <html>
                                                                <head>
                                                                    <meta charset=''utf - 8'' />
                                                                    <title> Hazards Identified (FLHA)/Risk Understood - At Risk </title>
                                                                </head>
                                                                <body>
                                                                    <h3 style=''margin-bottom:-10px;''>SOC NOTIFICATION</h3>
                                                                    <h4>Work zone inrusion - At Risk</h4>
                                                                    <a href=''https://www.google.com/maps/search/?api=1&query={0},{1}''>
                                                                    <img border=''0'' 
                                                                        src=''https://maps.googleapis.com/maps/api/staticmap?center={0},{1}&zoom=18&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:*%7C{0},{1}&key=AIzaSyAYrQoDLURs6EmTxSQ4VAtlNjJy9QhwBwA'' 
                                                                        alt=''Work Zone Intrusion - At Risk location''></a> </br></br>
                                                                    <span><strong>Location:</strong> {2}</span></br>
                                                                    <span><strong>Date/Time observed:</strong> {3}</br>
                                                                    <span><a href=''{4}''>View safety observation card</a></span></br></br></br>
																	<span>Sent from a non monitored mailbox. </span>
                                                                </body>
                                                            </html>', 'SOC Notification', 1),
(1, 'Pre Trip Inspection/Daily Job Briefing - At Risk', '<!DOCTYPE html>
                                                            <html>
                                                                <head>
                                                                    <meta charset=''utf - 8'' />
                                                                    <title> Pre Trip Inspection/Daily Job Briefing - At Risk </title>
                                                                </head>
                                                                <body>
                                                                    <h3 style=''margin-bottom:-10px;''>SOC NOTIFICATION</h3>
                                                                    <h4>Work zone inrusion - At Risk</h4>
                                                                    <a href=''https://www.google.com/maps/search/?api=1&query={0},{1}''>
                                                                    <img border=''0'' 
                                                                        src=''https://maps.googleapis.com/maps/api/staticmap?center={0},{1}&zoom=18&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:*%7C{0},{1}&key=AIzaSyAYrQoDLURs6EmTxSQ4VAtlNjJy9QhwBwA'' 
                                                                        alt=''Work Zone Intrusion - At Risk location''></a> </br></br>
                                                                    <span><strong>Location:</strong> {2}</span></br>
                                                                    <span><strong>Date/Time observed:</strong> {3}</br>
                                                                    <span><a href=''{4}''>View safety observation card</a></span></br></br></br>
																	<span>Sent from a non monitored mailbox. </span>
                                                                </body>
                                                            </html>', 'SOC Notification', 1)