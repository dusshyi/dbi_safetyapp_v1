﻿using DBI.Safety.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Services
{
    interface IAccountService
    {
        Task<bool> SignIn(HttpContext httpContext, LogInViewModel user);
        void SignOut(HttpContext httpContext);
    }
}
