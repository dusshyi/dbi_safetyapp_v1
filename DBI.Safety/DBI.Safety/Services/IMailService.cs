﻿using DBI.Safety.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Services
{
    interface IMailService
    {
        void SendWorkIntrusions(EmailForm form);
        void Send(EmailForm form);
    }
}
