﻿using Dapper;
using DBI.Safety.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DBI.Safety.Services
{
    public class AccountService : IAccountService
    {
        private readonly IConfiguration configuration;
        public AccountService(IConfiguration config)
        {
            this.configuration = config;
        }

        public async Task<bool> SignIn(HttpContext httpContext, LogInViewModel form)
        {
            bool _authenticated = false;
            using (PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "dbiservices.com"))
            {
                _authenticated = ctx.ValidateCredentials(form.Username + "@dbiservices.com", form.Password, ContextOptions.Negotiate);
                if (_authenticated)
                {
                    var identity = new ClaimsIdentity(this.GetUserClaims(form), CookieAuthenticationDefaults.AuthenticationScheme);

                    await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity));
                }
                return _authenticated;
            }
        }

        public async void SignOut(HttpContext httpContext)
        {
            await httpContext.SignOutAsync();
        }

        private IEnumerable<Claim> GetUserClaims(LogInViewModel form)
        {
            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.Name, form.Username));
            claims.Add(new Claim(ClaimTypes.Email, form.Username + "dbiservices.com"));
            claims.AddRange(this.GetUserRoleClaims(form));
            return claims;
        }

        private IEnumerable<Claim> GetUserRoleClaims(LogInViewModel form)
        {
            SqlConnection unifiedConn = new SqlConnection();
            unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
            using (IDbConnection dbConnection = unifiedConn)
            {
                dbConnection.Open();
                List<string> roleList = dbConnection.Query<string>(@"SELECT RoleDesc
                    FROM dbo.ApplicationsToUserAccounts p
	                    INNER JOIN dbo.UserAccounts ua ON ua.WindowsAccount = p.WindowsAccount
	                    INNER JOIN dbo.ApplicationsRoles r ON r.ApplicationRoleID = p.ApplicationRoleID
                    WHERE p.WindowsAccount = @UserId", new { @UserId = form.Username }).ToList();

                dbConnection.Close();

                List<Claim> claims = new List<Claim>();

                foreach (var role in roleList)
                {
                    claims.Add(new Claim(ClaimTypes.Role, role));
                }
                return claims;
            }
        }
    }
}
