﻿using DBI.Safety.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using Microsoft.AspNetCore.Hosting;

namespace DBI.Safety.Services
{
    public class LocalMailService : IMailService
    {
        private readonly IConfiguration configuration;
        private readonly IHostingEnvironment environment;
        private string defaultConnectionString { get; set; }
        public LocalMailService(IConfiguration config, IHostingEnvironment env)
        {
            this.configuration = config;

            if (environment.EnvironmentName == "Development" || environment.EnvironmentName == "Staging")
            {
                this.defaultConnectionString = "DefaultStagingConnection";
            }
            else
            {
                this.defaultConnectionString = "DefaultConnection";
            }
        }

        public void SendWorkIntrusions(EmailForm form)
        {
            SmtpClient client = new SmtpClient("smtp.dbiservices.com");
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("username", "password");

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(form.From);
            mailMessage.To.Add(form.To);
            //mailMessage.To.Add("dusshyi.subram@dbiservices.com");
            //mailMessage.To.Add("kelley.baranuk@dbiservices.com");
            //mailMessage.To.Add("patrick.owen@dbiservices.com");
            //mailMessage.To.Add("Lyndsay.Sutton@dbiservices.com");
            //mailMessage.To.Add("jconroy@dbiservices.com");
            mailMessage.Body = form.Body;
            mailMessage.Subject = form.Subject;
            mailMessage.IsBodyHtml = true;
            client.Send(mailMessage);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);

            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                using (var trans = dbConnection.BeginTransaction())
                {
                    try
                    {
                        dbConnection.Execute(@"INSERT INTO EmailLog (EmailId, ReceivedBy, SentBy, CreatedDate, Timestamp)
                                        Values(@emailId, @receivedBy, @sentBy, @createdDate, @timestamp)", new
                        {
                            @emailId = form.EmailId,
                            @receivedBy = form.To,
                            @sentBy = form.From,
                            @createdDate = DateTimeOffset.Now,
                            @timestamp = DateTimeOffset.Now
                        }, trans);

                        trans.Commit();
                        conn.Close();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        conn.Close();
                    }
                }
            }
        }

        public void Send(EmailForm form)
        {
            SmtpClient client = new SmtpClient("smtp.dbiservices.com");
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("username", "password");

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(form.From);
            mailMessage.To.Add(form.To);           
            mailMessage.Body = form.Body;
            mailMessage.Subject = form.Subject;
            mailMessage.IsBodyHtml = true;
            client.Send(mailMessage);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);

            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                using (var trans = dbConnection.BeginTransaction())
                {
                    try
                    {
                        dbConnection.Execute(@"INSERT INTO EmailLog (EmailId, ReceivedBy, SentBy, CreatedDate, Timestamp)
                                        Values(@emailId, @receivedBy, @sentBy, @createdDate, @timestamp)", new
                        {
                            @emailId = form.EmailId,
                            @receivedBy = form.To,
                            @sentBy = form.From,
                            @createdDate = DateTimeOffset.Now,
                            @timestamp = DateTimeOffset.Now
                        }, trans);

                        trans.Commit();
                        conn.Close();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        conn.Close();
                    }
                }
            }
        }
    }
}
