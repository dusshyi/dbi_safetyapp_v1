﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using DBI.Safety.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using DBI.Safety.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Hosting;

namespace DBI.Safety.Controllers
{
    [Authorize]
    public class SOCController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly IHostingEnvironment environment;
        private string defaultConnectionString { get; set; }
        private string appSafetyDb { get; set; }
        public SOCController(IConfiguration config, IHostingEnvironment env)
        {
            this.configuration = config;
            this.environment = env;

            if (environment.EnvironmentName == "Development" || environment.EnvironmentName == "Staging")
            {
                this.defaultConnectionString = "DefaultStagingConnection";
                this.appSafetyDb = "AppSafetyTestDb";
            }
            else
            {
                this.defaultConnectionString = "DefaultConnection";
                this.appSafetyDb = "AppSafetyDb";
            }
        }        

        public JsonResult GetTaskList(string locationId)
        {
            SqlConnection unifiedConn = new SqlConnection();
            unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
            using (IDbConnection dbConnection = unifiedConn)
            {
                dbConnection.Open();

                var userInfo = dbConnection.Query<dynamic>
                    ("App_sel_UserAccounts2", new
                    {
                        WindowsAccount = this.User.Identity.Name
                    },
                    commandType: CommandType.StoredProcedure).FirstOrDefault();

                List<SelectListItemV2> taskList = new List<SelectListItemV2>();

                if (userInfo != null)
                {
                    if (userInfo.BusinessUnit == "11 - DBI")
                    {
                        var locationName = dbConnection.Query<string>(@"SELECT location
                                  FROM [AppUnifiedCatalogDb].[dbo].[DARProjects]
                                  WHERE segment1 = @LocationId", new { @LocationId = locationId }).FirstOrDefault();

                        taskList = dbConnection.Query<SelectListItemV2>
                        ("App_Sel_ValueNamePair", new
                        {
                            pairName = "DARTask_ByLocation",
                            IntVar1 = 0,
                            IntVar2 = 0,
                            StrVar1 = locationName,
                            StrVar2 = ""
                        },
                        commandType: CommandType.StoredProcedure).ToList();
                    }
                    else if (userInfo.BusinessUnit == "41 - DBIS")
                    {
                        taskList = dbConnection.Query<SelectListItemV2>
                        ("App_Sel_ValueNamePair", new
                        {
                            pairName = "DARTask_BySegment1",
                            IntVar1 = 0,
                            IntVar2 = 0,
                            StrVar1 = locationId,
                            StrVar2 = ""
                        },
                        commandType: CommandType.StoredProcedure).ToList();
                    }

                    taskList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "Select" });
                    taskList.Insert(1, new SelectListItemV2 { ValueMember = "34", DisplayMember = "Office Work" });
                    taskList.Insert(2, new SelectListItemV2 { ValueMember = "35", DisplayMember = "Shop Work" });
                }

                dbConnection.Close();
                
                return Json(new SelectList(taskList, "ValueMember", "DisplayMember"));
            }            
        }

        public JsonResult GetTaskFilterList(int locationId)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                List<SelectListItemV2> taskList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT s.Task AS ValueMember, '(' + t.task_number + ')' +
                    CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS DisplayMember                    
                    FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON s.Task = t.task_id WHERE s.Location = @location", new { @Location = locationId }).ToList();                
                dbConnection.Close();

                taskList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                taskList.Insert(1, new SelectListItemV2 { ValueMember = "34", DisplayMember = "Office Work" });
                taskList.Insert(2, new SelectListItemV2 { ValueMember = "35", DisplayMember = "Shop Work" });
                return Json(new SelectList(taskList, "ValueMember", "DisplayMember"));
            }
        }

        public SOCForm GetSOCForm(SOCForm activeForm = null)
        {
            SOCForm form = new SOCForm();

            SqlConnection unifiedConn = new SqlConnection();
            unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
            using (IDbConnection dbConnection = unifiedConn)
            {
                dbConnection.Open();

                var userInfo = dbConnection.Query<dynamic>
                    ("App_sel_UserAccounts2", new
                    {
                        WindowsAccount = this.User.Identity.Name
                    },
                    commandType: CommandType.StoredProcedure).FirstOrDefault();

                List<SelectListItemV2> locationList = new List<SelectListItemV2>();
                string locationProcName = string.Empty;

                if (userInfo != null)
                {                    
                    if (userInfo.BusinessUnit == "11 - DBI")
                    {
                        locationProcName = "DARProjects_ByEmployee2";                                      
                    } 
                    else if(userInfo.BusinessUnit == "41 - DBIS")
                    {
                        locationProcName = "DARProjects_ByEmployee3b"; 
                    }
                }

                locationList = dbConnection.Query<SelectListItemV2>
                    ("App_Sel_ValueNamePair", new
                    {
                        pairName = locationProcName,
                        IntVar1 = 0,
                        IntVar2 = 0,
                        StrVar1 = this.User.Identity.Name,
                        StrVar2 = "SafetyApp"
                    },
                    commandType: CommandType.StoredProcedure).ToList();

                locationList.Insert(0, new SelectListItemV2()
                {
                    ValueMember = "0",
                    DisplayMember = "Please select location"
                });

                form.LocationList = locationList;

                if (activeForm != null)
                {
                    if (!string.IsNullOrEmpty(activeForm.SelectedLocationId))
                    {
                        List<SelectListItemV2> taskList = new List<SelectListItemV2>();

                        if (userInfo != null)
                        {
                            if (userInfo.BusinessUnit == "11 - DBI")
                            {
                                var locationName = dbConnection.Query<string>(@"SELECT location
                                  FROM [AppUnifiedCatalogDb].[dbo].[DARProjects]
                                  WHERE segment1 = @LocationId", new { @LocationId = activeForm.SelectedLocationId }).FirstOrDefault();

                                taskList = dbConnection.Query<SelectListItemV2>
                                ("App_Sel_ValueNamePair", new
                                {
                                    pairName = "DARTask_ByLocation",
                                    IntVar1 = 0,
                                    IntVar2 = 0,
                                    StrVar1 = locationName,
                                    StrVar2 = ""
                                },
                                commandType: CommandType.StoredProcedure).ToList();
                            }
                            else if (userInfo.BusinessUnit == "41 - DBIS")
                            {
                                taskList = dbConnection.Query<SelectListItemV2>
                                ("App_Sel_ValueNamePair", new
                                {
                                    pairName = "DARTask_BySegment1",
                                    IntVar1 = 0,
                                    IntVar2 = 0,
                                    StrVar1 = activeForm.SelectedLocationId,
                                    StrVar2 = ""
                                },
                                commandType: CommandType.StoredProcedure).ToList();
                            }

                            taskList.Insert(0, new SelectListItemV2() { ValueMember = "0", DisplayMember = "Please select task" });
                            taskList.Insert(1, new SelectListItemV2 { ValueMember = "34", DisplayMember = "Office Work" });
                            taskList.Insert(2, new SelectListItemV2 { ValueMember = "35", DisplayMember = "Shop Work" });
                        }
                        
                        form.TaskList = taskList;
                    }
                    else
                    {
                        List<SelectListItemV2> taskList = new List<SelectListItemV2>();
                        taskList.Insert(0, new SelectListItemV2() { ValueMember = "0", DisplayMember = "Please select task" });
                        form.TaskList = taskList;
                    }
                } 
                else
                {
                    List<SelectListItemV2> taskList = new List<SelectListItemV2>();
                    taskList.Insert(0, new SelectListItemV2() { ValueMember = "0", DisplayMember = "Please select task" });
                    form.TaskList = taskList;
                }                

                dbConnection.Close();
            }

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);

            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                var questions = dbConnection.Query<QuestionsFormViewModel>(@"SELECT d.Id, d.Name, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id 
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'SOC' AND c.Description = 'Question' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY c.SortOrderId, d.SortOrderId");
                form.Questions = new List<QuestionsFormViewModel>();

                foreach (var item in questions)
                {
                    var possibleAnswers = dbConnection.Query<dynamic>(
                    @"SELECT d.Id, d.Name AS Text
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'SOC' AND c.Name = 'Answer' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1").ToList();

                    var possibleAnswerList = new List<Answer>();
                    foreach (var aItem in possibleAnswers)
                    {
                        possibleAnswerList.Add(new Answer { Id = aItem.Id, Text = aItem.Text });
                    }

                    if (activeForm != null)
                    {
                        foreach (var q in activeForm.Questions)
                        {
                            if (item.Id == q.Id)
                            {
                                form.Questions.Add(new QuestionsFormViewModel
                                {
                                    Id = item.Id,
                                    Name = item.Name,
                                    Category = item.Category,
                                    SortOrderId = item.SortOrderId,
                                    PossibleAnswers = possibleAnswerList,
                                    SelectedAnswer = q.SelectedAnswer
                                });
                                break;
                            }
                        }
                        form.IsQuestionSelected = activeForm.IsQuestionSelected;
                        form.IsLocationEnabled = activeForm.IsLocationEnabled;

                        if(activeForm.SelectedObserverType != null)
                        {
                            form.IsObserverTypeSelected = false;
                        } else
                        {
                            form.IsObserverTypeSelected = true;
                        }              
                    }
                    else
                    {
                        form.Questions.Add(new QuestionsFormViewModel
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Category = item.Category,
                            SortOrderId = item.SortOrderId,
                            PossibleAnswers = possibleAnswerList
                        });
                        form.IsQuestionSelected = true;
                        form.IsObserverTypeSelected = false;

                        if (!string.IsNullOrEmpty(Request.Cookies["latitude"]) && !string.IsNullOrEmpty(Request.Cookies["longitude"]) &&
                        !string.IsNullOrEmpty(Request.Cookies["accuracy"]))
                        {
                            form.IsLocationEnabled = true;
                        }
                        else
                        {
                            form.IsLocationEnabled = false;
                        }                        
                    }
                }

                form.WorkZoneIntrusionsSpecialQuestion = dbConnection.Query<Modal>(
                    @"SELECT d.Name AS Body, c.Name AS Header
                      FROM Dictionary d
                      INNER JOIN Category c ON d.CategoryId = c.Id
                      INNER JOIN Form f ON c.FormId = f.Id
                      WHERE f.Name = 'SOC' AND c.Name = 'Work Zone Intrusions' AND
                      d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1").FirstOrDefault();

                form.WorkZoneIntrusionsConfirmation = dbConnection.Query<Modal>(
                    @"SELECT d.Description AS Body, c.Name + ' Confirmation' AS Header
                      FROM Dictionary d
                      INNER JOIN Category c ON d.CategoryId = c.Id
                      INNER JOIN Form f ON c.FormId = f.Id
                      WHERE f.Name = 'SOC' AND c.Name = 'Work Zone Intrusions' AND
                      d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1").FirstOrDefault();

                var possibleObserverType = dbConnection.Query<dynamic>(
                    @"SELECT d.Id, d.Name AS Text
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'SOC' AND c.Name = 'Observer Title' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1").ToList();

                var possibleObserverTypeList = new List<ObserverType>();
                foreach (var pItem in possibleObserverType)
                {
                    possibleObserverTypeList.Add(new ObserverType { Id = pItem.Id, Name = pItem.Text });
                }
                form.ObserverType = possibleObserverTypeList;

                form.Observer = this.User.Identity.Name;

                return form;
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewData["Message"] = "Fill your safety observation card";

            return View(GetSOCForm());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SOCForm form)
        {
            foreach (var item in form.Questions)
            {
                if (!item.SelectedAnswer.HasValue)
                {
                    form.IsQuestionSelected = false;
                    break;
                }
                else
                {
                    form.IsQuestionSelected = true;
                }
            }

            if (!string.IsNullOrEmpty(Request.Cookies["latitude"]) && !string.IsNullOrEmpty(Request.Cookies["longitude"]) &&
                        !string.IsNullOrEmpty(Request.Cookies["accuracy"]))
            {
                form.IsLocationEnabled = true;
            }
            else
            {
                form.IsLocationEnabled = false;
            }

            if (form.SelectedObserverType != null)
            {
                form.IsObserverTypeSelected = false;
            }
            else
            {
                form.IsObserverTypeSelected = true;
            }

            if (form.IsQuestionSelected)
            {
                if (ModelState.IsValid)
                {
                    string locationId = string.Empty;
                    string locationName = string.Empty;
                    string projectName = string.Empty;
                    string projectManagerEmail = string.Empty;

                    SqlConnection unifiedConn = new SqlConnection();
                    unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
                    using (IDbConnection dbConnection = unifiedConn)
                    {
                        dbConnection.Open();

                        locationId = dbConnection.Query<string>(@"SELECT project_id
                        FROM DARProjects 
                        WHERE segment1 = @Id", new { @Id = form.SelectedLocationId }).FirstOrDefault();

                        locationName = dbConnection.Query<string>(@"SELECT location AS Location
                        FROM DARProjects 
                        WHERE segment1 = @Id", new { @Id = form.SelectedLocationId }).FirstOrDefault();

                        var region = dbConnection.Query<string>(@"SELECT Region
                        FROM DARProjects 
                        WHERE segment1 = @Id", new { @Id = form.SelectedLocationId }).FirstOrDefault();
                        form.Region = region;

                        projectName = dbConnection.Query<string>(@"SELECT '(' + segment1 + ')' + ' ' + long_name
                        FROM DARProjects 
                        WHERE segment1 = @Id", new { @Id = form.SelectedLocationId }).FirstOrDefault();

                        projectManagerEmail = dbConnection.Query<string>(@"SELECT Email_Address
                        FROM ProjectManagers pm
                        INNER JOIN DARProjects p ON pm.Project_Number = p.segment1
                        Where p.segment1 = @Id", new { @Id = form.SelectedLocationId }).FirstOrDefault();

                        dbConnection.Close();
                    }

                    SqlConnection conn = new SqlConnection();
                    conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);

                    if (form.IsLocationEnabled)
                    {
                        if (!form.IsObserverTypeSelected)
                        {
                            using (IDbConnection dbConnection = conn)
                            {
                                dbConnection.Open();
                                var observationType = conn.Query<string>(@"SELECT Name FROM Dictionary WHERE Id = @Id",
                                           new { @Id = form.SelectedObserverType.Value }).FirstOrDefault();

                                using (var trans = dbConnection.BeginTransaction())
                                {
                                    try
                                    {
                                        var socId = 0;
                                        socId = conn.Query<int>(@"INSERT INTO SOCDataV2 (OtherAtRiskData, Comments, Location, LocationName, Region, Observer, Task, 
                                    OtherPositiveObservation, ObservedBy, Latitude, Longitude, Accuracy, CreatedBy, CreatedDate, Timestamp)
                                    Values(@otherAtRiskData, @comments, @location, @LocationName, @region, @observer, @task, @otherPositiveObservation, @observedBy, 
                                    @latitude, @longitude, @accuracy, @createdBy, @createdDate, @timestamp) 
                                    SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY];", new
                                        {
                                            @otherAtRiskData = form.OtherAtRiskData,
                                            @comments = form.Comments,
                                            @location = locationId,
                                            @LocationName = locationName,
                                            @region = form.Region,
                                            @observer = this.User.Identity.Name,
                                            @task = form.SelectedTaskId,
                                            @otherPositiveObservation = form.OtherPositiveObservation,
                                            @observedBy = observationType,
                                            @latitude = Convert.ToDecimal(Request.Cookies["latitude"]),
                                            @longitude = Convert.ToDecimal(Request.Cookies["longitude"]),
                                            @accuracy = Convert.ToInt32(Math.Round(Convert.ToDecimal(Request.Cookies["accuracy"]), 0)),
                                            @createdBy = this.User.Identity.Name,
                                            @createdDate = DateTimeOffset.Now,
                                            @timestamp = DateTimeOffset.Now
                                        }, trans).SingleOrDefault();

                                        var workZoneIntrusionsAtRisk = false;
                                        var hazardsIdentifiedAtRisk = false;
                                        var preTripAtRisk = false;
                                        foreach (var item in form.Questions)
                                        {
                                            if (item.Name.Contains("Work Zone Intrusions") && item.SelectedAnswer == 68)
                                            {
                                                workZoneIntrusionsAtRisk = true;
                                            }
                                            else if (item.Name.Contains("Hazards Identified (FLHA)/Risk Understood") && item.SelectedAnswer == 68)
                                            {
                                                hazardsIdentifiedAtRisk = true;
                                            }
                                            else if (item.Name.Contains("Pre Trip Inspection/Daily Job Briefing") && item.SelectedAnswer == 68)
                                            {
                                                preTripAtRisk = true;
                                            }

                                            dbConnection.Execute(@"INSERT INTO SOCDataItemV2 (SOCDataId, QuestionId, AnswerId, CreatedBy,
                                        CreatedDate, Timestamp)
                                        Values(@SOCDataId, @QuestionId, @AnswerId, @CreatedBy, @CreatedDate, @Timestamp)", new
                                            {
                                                @SOCDataId = socId,
                                                @QuestionId = item.Id,
                                                @AnswerId = item.SelectedAnswer,
                                                @createdBy = this.User.Identity.Name,
                                                @createdDate = DateTimeOffset.Now,
                                                @timestamp = DateTimeOffset.Now
                                            }, trans);
                                        }

                                        trans.Commit();
                                        conn.Close();

                                        if (workZoneIntrusionsAtRisk)
                                        {
                                            var emailTemplate = conn.Query<dynamic>(@"SELECT Id, [Subject], Body
                                        FROM Email
                                        WHERE Id = 1 AND IsActive = 1").FirstOrDefault();

                                            var regionalManagerEmail = conn.Query<string>(@"SELECT Email
                                        FROM RegionalManager
                                        WHERE Region = @region AND IsActive = 1", new { @region = form.Region }).FirstOrDefault();

                                            if (!string.IsNullOrEmpty(regionalManagerEmail))
                                            {
                                                EmailForm emailForm = new EmailForm();
                                                emailForm.EmailId = emailTemplate.Id;
                                                emailForm.To = regionalManagerEmail;
                                                emailForm.From = "SafetyApp@dbiservices.com";
                                                emailForm.Subject = emailTemplate.Subject;
                                                // {0} - Latitude
                                                // {1} - Longitude
                                                // {2} - Project Name
                                                // {3} - DateTime Entry
                                                // {4} - SOC Id
                                                emailForm.Body = string.Format(emailTemplate.Body,
                                                                    Convert.ToDecimal(Request.Cookies["latitude"]),
                                                                    Convert.ToDecimal(Request.Cookies["longitude"]),
                                                                    projectName,
                                                                    DateTimeOffset.Now,
                                                                    "https://safetyapp.dbiservices.com/SOC/History/" + socId);

                                                LocalMailService mail = new LocalMailService(configuration, environment);
                                                mail.SendWorkIntrusions(emailForm);
                                            }
                                        }

                                        if (hazardsIdentifiedAtRisk)
                                        {
                                            var emailTemplate = conn.Query<dynamic>(@"SELECT Id, [Subject], Body
                                        FROM Email
                                        WHERE Id = 2 AND IsActive = 1").FirstOrDefault();

                                            if (!string.IsNullOrEmpty(projectManagerEmail))
                                            {
                                                EmailForm emailForm = new EmailForm();
                                                emailForm.EmailId = emailTemplate.Id;
                                                emailForm.To = projectManagerEmail;
                                                emailForm.From = "SafetyApp@dbiservices.com";
                                                emailForm.Subject = emailTemplate.Subject;
                                                // {0} - Latitude
                                                // {1} - Longitude
                                                // {2} - Project Name
                                                // {3} - DateTime Entry
                                                // {4} - SOC Id
                                                emailForm.Body = string.Format(emailTemplate.Body,
                                                                    Convert.ToDecimal(Request.Cookies["latitude"]),
                                                                    Convert.ToDecimal(Request.Cookies["longitude"]),
                                                                    projectName,
                                                                    DateTimeOffset.Now,
                                                                    "https://safetyapp.dbiservices.com/SOC/History/" + socId);

                                                LocalMailService mail = new LocalMailService(configuration, environment);
                                                mail.Send(emailForm);
                                            }
                                        }

                                        if (preTripAtRisk)
                                        {
                                            var emailTemplate = conn.Query<dynamic>(@"SELECT Id, [Subject], Body
                                        FROM Email
                                        WHERE Id = 3 AND IsActive = 1").FirstOrDefault();

                                            if (!string.IsNullOrEmpty(projectManagerEmail))
                                            {
                                                EmailForm emailForm = new EmailForm();
                                                emailForm.EmailId = emailTemplate.Id;
                                                emailForm.To = projectManagerEmail;
                                                emailForm.From = "SafetyApp@dbiservices.com";
                                                emailForm.Subject = emailTemplate.Subject;
                                                // {0} - Latitude
                                                // {1} - Longitude
                                                // {2} - Project Name
                                                // {3} - DateTime Entry
                                                // {4} - SOC Id
                                                emailForm.Body = string.Format(emailTemplate.Body,
                                                                    Convert.ToDecimal(Request.Cookies["latitude"]),
                                                                    Convert.ToDecimal(Request.Cookies["longitude"]),
                                                                    projectName,
                                                                    DateTimeOffset.Now,
                                                                    "https://safetyapp.dbiservices.com/SOC/History/" + socId);

                                                LocalMailService mail = new LocalMailService(configuration, environment);
                                                mail.Send(emailForm);
                                            }
                                        }

                                        Response.Cookies.Delete("latitude");
                                        Response.Cookies.Delete("longitude");
                                        Response.Cookies.Delete("accuracy");
                                        return RedirectToAction("Index", "Home");
                                    }
                                    catch (Exception ex)
                                    {
                                        trans.Rollback();
                                        conn.Close();
                                        Response.Cookies.Delete("latitude");
                                        Response.Cookies.Delete("longitude");
                                        Response.Cookies.Delete("accuracy");
                                        return RedirectToAction("Error", "Home", new { message = ex });
                                    }
                                }
                            }
                        }
                        else
                        {
                            return View(GetSOCForm(form));
                        }
                    }
                    else
                    {
                        return View(GetSOCForm(form));
                    }
                }
                else
                {
                    return View(GetSOCForm(form));
                }
            }
            else
            {
                return View(GetSOCForm(form));
            }
        }

        [HttpGet]
        [Route("SOC/History")]
        public ActionResult History()
        {
            ViewData["Message"] = "Safety observation card logs";
            var isAdmin = this.User.IsInRole("Safety Admin");

            SOCHistoryForm form = new SOCHistoryForm();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();

                List<SelectListItemV2> regionList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT Region AS DisplayMember, Region AS ValueMember 
                    FROM SOCDataV2").ToList();
                regionList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                form.RegionList = regionList;

                List<SelectListItemV2> locationList = dbConnection.Query<SelectListItemV2>(@"SELECT  
                    MIN(s.Location) AS ValueMember,  p.location AS DisplayMember
                    FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON s.Location = p.project_id
                    INNER JOIN AppUnifiedCatalogDb.dbo.EmsPermissions ep ON ep.module_name = 'SafetyApp' AND ep.PROJECT_NUMBER = p.segment1
                    WHERE ep.USER_NAME = @UserId
                    GROUP BY p.Location", new { @UserId = this.User.Identity.Name }).ToList();
                locationList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                form.LocationList = locationList;

                if (!string.IsNullOrEmpty(form.SelectedLocationId))
                {
                    List<SelectListItemV2> taskList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT s.Task AS ValueMember, '(' + t.task_number + ')' +
                    CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS DisplayMember                    
                    FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON s.Task = t.task_id WHERE s.Location = @location", new { @Location = form.SelectedLocationId }).ToList();
                    taskList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                    taskList.Insert(1, new SelectListItemV2 { ValueMember = "34", DisplayMember = "Office Work" });
                    taskList.Insert(2, new SelectListItemV2 { ValueMember = "35", DisplayMember = "Shop Work" });
                    form.TaskList = taskList;
                }
                else
                {
                    List<SelectListItemV2> taskList = new List<SelectListItemV2>();
                    taskList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                    form.TaskList = taskList;
                }
                
                IEnumerable<SOCLog> logList = null;

                if (isAdmin)
                {
                    logList = dbConnection.Query<SOCLog>(@"SELECT s.Id, s.CreatedBy, s.CreatedDate, s.LocationName AS Location, s.Region,
	                    '(' + t.task_number + ')' +
                                        CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Task
                        FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON s.Location = p.project_id          
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON s.Task = t.task_id
                        WHERE IsDeleted = 0 
                        ORDER BY s.CreatedDate DESC");
                }
                else
                {
                    logList = dbConnection.Query<SOCLog>(@"SELECT s.Id, s.CreatedBy, s.CreatedDate, s.LocationName AS Location, s.Region,
	                    '(' + t.task_number + ')' +
                                        CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Task
                        FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON s.Location = p.project_id
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON s.Task = t.task_id
                        LEFT JOIN " + appSafetyDb + @".dbo.RegionalManager rm ON s.Region = rm.Region
                        LEFT JOIN AppUnifiedCatalogDb.dbo.ProjectManagers pm ON p.segment1 = pm.Project_Number
                        WHERE IsDeleted = 0 AND (s.CreatedBy = @user OR rm.Email = @userEmail OR 
                        pm.Email_Address = @userEmail) ORDER BY s.CreatedDate DESC", new
                    {
                        @user = this.User.Identity.Name,
                        @userEmail = this.User.Identity.Name + "dbiservices.com"
                    });
                }

                form.SOCLogs = new List<SOCLog>();
                foreach (var item in logList)
                {
                    form.SOCLogs.Add(new SOCLog
                    {
                        Id = item.Id,
                        CreatedBy = item.CreatedBy,
                        Location = item.Location,
                        Task = item.Task,
                        Region = item.Region,
                        CreatedDate = item.CreatedDate
                    });
                }
            }

            return View(form);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult History(SOCHistoryForm form)
        {
            ViewData["Message"] = "Safety observation card logs";
            var isAdmin = this.User.IsInRole("Safety Admin");

            if(form.To != null)
            {
                form.To = form.To.Value.AddDays(1);
            }            

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();

                List<SelectListItemV2> regionList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT Region AS DisplayMember, Region AS ValueMember 
                    FROM SOCDataV2").ToList();
                regionList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                form.RegionList = regionList;

                List<SelectListItemV2> locationList = dbConnection.Query<SelectListItemV2>(@"SELECT  
                    MIN(s.Location) AS ValueMember,  p.location AS DisplayMember
                    FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON s.Location = p.project_id
                    INNER JOIN AppUnifiedCatalogDb.dbo.EmsPermissions ep ON ep.module_name = 'SafetyApp' AND ep.PROJECT_NUMBER = p.segment1
                    WHERE ep.USER_NAME = @UserId
                    GROUP BY p.Location", new { @UserId = this.User.Identity.Name }).ToList();
                locationList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                form.LocationList = locationList;

                if (!string.IsNullOrEmpty(form.SelectedLocationId))
                {
                    List<SelectListItemV2> taskList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT s.Task AS ValueMember, '(' + t.task_number + ')' +
                    CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS DisplayMember                    
                    FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON s.Task = t.task_id WHERE s.Location = @location", new { @Location = form.SelectedLocationId }).ToList();
                    taskList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                    taskList.Insert(1, new SelectListItemV2 { ValueMember = "34", DisplayMember = "Office Work" });
                    taskList.Insert(2, new SelectListItemV2 { ValueMember = "35", DisplayMember = "Shop Work" });
                    form.TaskList = taskList;
                }
                else
                {
                    List<SelectListItemV2> taskList = new List<SelectListItemV2>();
                    taskList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                    form.TaskList = taskList;
                }

                List<SOCLog> logList = new List<SOCLog>();
                var sql = string.Empty;
                var sqlParameters = string.Empty;
                string projectManagerEmail = string.Empty;
                string regionalManagerEmail = string.Empty;

                if (form.SelectedLocationId == "0" && form.SelectedTaskId == "0" && 
                    form.SelectedRegionId == "0" && form.From == null && form.To == null)
                {                    
                    if (isAdmin)
                    {
                        // Remove the location for LocationName
                        // '(' + p.segment1 + ')' + ' ' + p.long_name AS Location
                        sql = @"SELECT s.Id, s.CreatedBy, s.CreatedDate, s.LocationName AS Location, s.Region,
	                    '(' + t.task_number + ')' +
                                        CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Task
                        FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON s.Location = p.project_id          
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON s.Task = t.task_id
                        WHERE IsDeleted = 0 
                        ORDER BY s.CreatedDate DESC";
                    }
                    else
                    {
                        sql = @"SELECT s.Id, s.CreatedBy, s.CreatedDate, s.LocationName AS Location, s.Region,
	                    '(' + t.task_number + ')' +
                                        CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Task
                        FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON s.Location = p.project_id
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON s.Task = t.task_id
                        LEFT JOIN " + appSafetyDb + @".dbo.RegionalManager rm ON s.Region = rm.Region
                        LEFT JOIN AppUnifiedCatalogDb.dbo.ProjectManagers pm ON p.segment1 = pm.Project_Number
                        WHERE IsDeleted = 0 AND (s.CreatedBy = @user OR rm.Email = @userEmail OR 
                        pm.Email_Address = @userEmail) ORDER BY s.CreatedDate DESC";
                    }                    

                    logList = dbConnection.Query<SOCLog>(sql, new { @user = this.User.Identity.Name, @userEmail = this.User.Identity.Name + "@dbiservices.com" }).ToList();
                }
                else
                {
                    if (form.SelectedLocationId != "0")
                    {
                        sqlParameters = "s.Location = @Location";
                    }
                    if (form.SelectedTaskId != "0")
                    {
                        if (string.IsNullOrEmpty(sqlParameters))
                        {
                            sqlParameters = "s.Task = @Task";
                        }
                        else
                        {
                            sqlParameters = sqlParameters + " AND s.Task = @Task";
                        }
                    }
                    if (form.SelectedRegionId != "0")
                    {
                        if (string.IsNullOrEmpty(sqlParameters))
                        {
                            sqlParameters = "s.Region = @Region";
                        }
                        else
                        {
                            sqlParameters = sqlParameters + " AND s.Region = @Region";
                        }
                    }
                    if (form.From != null && form.To != null)
                    {
                        if (string.IsNullOrEmpty(sqlParameters))
                        {
                            sqlParameters = "s.CreatedDate BETWEEN @from AND @to";
                        }
                        else
                        {
                            sqlParameters = sqlParameters + " AND s.CreatedDate BETWEEN @from AND @to";
                        }
                    }

                    if(string.IsNullOrEmpty(sqlParameters))
                    {
                        if (isAdmin)
                        {
                            sql = @"SELECT s.Id, s.CreatedBy, s.CreatedDate, s.LocationName AS Location, s.Region,
	                        '(' + t.task_number + ')' +
                                            CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Task
                            FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                            INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON s.Location = p.project_id          
                            INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON s.Task = t.task_id
                            WHERE IsDeleted = 0 
                            ORDER BY s.CreatedDate DESC";
                        }
                        else
                        {
                            sql = @"SELECT s.Id, s.CreatedBy, s.CreatedDate, s.LocationName AS Location, s.Region,
	                        '(' + t.task_number + ')' +
                                            CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Task
                            FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                            INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON s.Location = p.project_id
                            INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON s.Task = t.task_id
                            LEFT JOIN " + appSafetyDb + @".dbo.RegionalManager rm ON s.Region = rm.Region
                            LEFT JOIN AppUnifiedCatalogDb.dbo.ProjectManagers pm ON p.segment1 = pm.Project_Number
                            WHERE IsDeleted = 0 AND (s.CreatedBy = @user OR rm.Email = @userEmail OR 
                            pm.Email_Address = @userEmail) ORDER BY s.CreatedDate DESC";
                        }
                    }
                    else
                    {
                        if (isAdmin)
                        {
                            sql = @"SELECT s.Id, s.CreatedBy, s.CreatedDate, s.LocationName AS Location, s.Region,
	                        '(' + t.task_number + ')' +
                                            CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Task
                            FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                            INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON s.Location = p.project_id          
                            INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON s.Task = t.task_id
                            WHERE IsDeleted = 0 AND (" + sqlParameters + ") ORDER BY s.CreatedDate DESC";
                        }
                        else
                        {
                            sql = @"SELECT s.Id, s.CreatedBy, s.CreatedDate, s.LocationName AS Location, s.Region,
	                        '(' + t.task_number + ')' +
                                            CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Task
                            FROM " + appSafetyDb + @".dbo.SOCDataV2 s
                            INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON s.Location = p.project_id
                            INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON s.Task = t.task_id
                            LEFT JOIN " + appSafetyDb + @".dbo.RegionalManager rm ON s.Region = rm.Region
                            LEFT JOIN AppUnifiedCatalogDb.dbo.ProjectManagers pm ON p.segment1 = pm.Project_Number
                            WHERE IsDeleted = 0 AND (s.CreatedBy = @user OR rm.Email = @userEmail OR 
                            pm.Email_Address = @userEmail) AND (" + sqlParameters + ") ORDER BY s.CreatedDate DESC";
                        }
                    }

                    logList = dbConnection.Query<SOCLog>(sql, new
                    {
                        @Location = form.SelectedLocationId,
                        @Task = form.SelectedTaskId,
                        @Region = form.SelectedRegionId,
                        @from = (form.From == null) ? DateTimeOffset.Now : form.From,
                        @to = (form.To == null) ? DateTimeOffset.Now : form.To,
                        @user = this.User.Identity.Name,
                        @userEmail = this.User.Identity.Name + "@dbiservices.com"
                    }).ToList();
                }                

                form.SOCLogs = new List<SOCLog>();
                foreach (var item in logList)
                {
                    form.SOCLogs.Add(new SOCLog
                    {
                        Id = item.Id,
                        CreatedBy = item.CreatedBy,
                        Location = item.Location,
                        CreatedDate = item.CreatedDate,
                        Region = item.Region,
                        Task = item.Task
                    });
                }

                return View(form);
            }
        }

        [HttpGet]
        [Route("SOC/History/{id}")]
        public ActionResult LogCard(int id)
        {
            ViewData["Message"] = "Safety observation card logs";

            SOCViewModel soc = new SOCViewModel();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();

                var socData = dbConnection.Query<SOCViewModel>(@"SELECT OtherAtRiskData, Comments, LocationName AS Location, Observer, 
                    CreatedDate, Task, OtherPositiveObservation, ObservedBy, CreatedBy
                    FROM SOCDataV2 
                    WHERE IsDeleted = 0 AND Id = @Id", new { @Id = id }).FirstOrDefault();
                soc = socData;

                var questions = dbConnection.Query<QuestionsViewModel>(@"SELECT Id, QuestionId, AnswerId 
                    FROM SOCDataItemV2 
                    WHERE SOCDataId = @SOCDataId", new { @SOCDataId = id });
                soc.Questions = new List<QuestionsViewModel>();

                soc.ObserverTypePrint = dbConnection.Query<ObserverType>(
                   @"SELECT d.Id, d.Name
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'SOC' AND c.Name = 'Observer Title' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1").ToList();

                foreach (var item in questions)
                {
                    var question = dbConnection.Query<dynamic>(
                    @"SELECT d.Name, c.Name AS Category
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    WHERE d.Id = @questionId AND d.IsActive = 1 AND c.IsActive = 1",
                    new { @questionId = item.QuestionId }).FirstOrDefault();

                    var answer = dbConnection.Query<dynamic>(
                    @"SELECT d.Name
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    WHERE d.Id = @answerId AND d.IsActive = 1 AND c.IsActive = 1",
                    new { @answerId = item.AnswerId }).FirstOrDefault();

                    soc.Questions.Add(new QuestionsViewModel
                    {
                        Id = item.Id,
                        Name = question.Name,
                        Category = question.Category,
                        SelectedAnswer = answer.Name
                    });
                }
            }

            SqlConnection unifiedConn = new SqlConnection();
            unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
            using (IDbConnection dbConnection = unifiedConn)
            {
                dbConnection.Open();
                var taskName = dbConnection.Query<string>(@"SELECT '(' + task_number + ')' +
                    CASE WHEN LEN(task_description) > 0 THEN ' - ' +  task_description ELSE '' END
                    FROM DARTask 
                    WHERE task_id = @Id", new { @Id = soc.Task }).FirstOrDefault();
                soc.Task = taskName;

                dbConnection.Close();
            }

            return View(soc);
        }                
    }
}