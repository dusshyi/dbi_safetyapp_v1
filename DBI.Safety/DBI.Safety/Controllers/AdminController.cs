﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DBI.Safety.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DBI.Safety.Controllers
{
    [Authorize(Roles = "Safety Admin")]
    public class AdminController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly IHostingEnvironment environment;
        private string defaultConnectionString { get; set; }
        public AdminController(IConfiguration config, IHostingEnvironment env)
        {
            this.configuration = config;
            this.environment = env;

            if (environment.EnvironmentName == "Development" || environment.EnvironmentName == "Staging")
            {
                this.defaultConnectionString = "DefaultStagingConnection";
            }
            else
            {
                this.defaultConnectionString = "DefaultConnection";
            }
        }

        public IActionResult AddForm()
        {
            ViewData["Message"] = "Add new form";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddForm(AddSavetyForm form)
        {
            ViewData["Message"] = "Add new form";

            if (ModelState.IsValid)
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
                using (IDbConnection dbConnection = conn)
                {
                    dbConnection.Open();
                    using (var trans = dbConnection.BeginTransaction())
                    {
                        try
                        {
                            dbConnection.Execute(@"INSERT INTO Form (Name, Description, IsActive, [CreatedBy], [CreatedDate], [Timestamp])
                                Values(@name, @description, @isActive, @createdBy, @createdDate, @timestamp)", new
                            {
                                @name = form.Name,
                                @description = form.Description,
                                @isActive = 1,
                                @createdBy = this.User.Identity.Name,
                                @createdDate = DateTimeOffset.Now,
                                @timestamp = DateTimeOffset.Now
                            }, trans);

                            trans.Commit();
                            conn.Close();
                            return RedirectToAction("Index", "Home");
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            conn.Close();
                            return RedirectToAction("Error", ex);
                        }
                    }
                }
            }

            return View(form);
        }

        public IActionResult AddCategory()
        {
            ViewData["Message"] = "Add new category";

            AddCategoryForm form = new AddCategoryForm();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                List<SelectListItemV1> formList = dbConnection.Query<SelectListItemV1>(
                    @"SELECT Id AS Value, Name AS Text, [Description]
                    FROM Form
                    WHERE IsActive = 1"
                ).ToList();

                formList.Insert(0, new SelectListItemV1()
                {
                    Text = "Please select form",
                    Value = 0
                });

                form.FormList = formList;

                return View(form);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddCategory(AddCategoryForm form)
        {
            ViewData["Message"] = "Add new category";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);

            if (ModelState.IsValid)
            {                
                using (IDbConnection dbConnection = conn)
                {
                    dbConnection.Open();
                    using (var trans = dbConnection.BeginTransaction())
                    {
                        try
                        {
                            dbConnection.Execute(@"INSERT INTO Category (FormId, Name, Description, IsActive, SortOrderId, [CreatedBy], [CreatedDate], [Timestamp])
                                Values(@formId, @name, @description, @isActive, @sortOrderId, @createdBy, @createdDate, @timestamp)", new
                            {
                                @formId = form.SelectedFormId,
                                @name = form.Name,
                                @description = form.Description,
                                @isActive = 1,
                                @sortOrderId = form.SortOrderId,
                                @createdBy = this.User.Identity.Name,
                                @createdDate = DateTimeOffset.Now,
                                @timestamp = DateTimeOffset.Now
                            }, trans);

                            trans.Commit();
                            conn.Close();
                            return RedirectToAction("Index", "Home");
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            conn.Close();
                            return RedirectToAction("Error", ex);
                        }
                    }
                }
            }

            using (IDbConnection dbConnection = conn)
            {
                List<SelectListItemV1> formList = dbConnection.Query<SelectListItemV1>(
                    @"SELECT Id AS Value, Name AS Text, [Description]
                    FROM Form
                    WHERE IsActive = 1"
                ).ToList();

                formList.Insert(0, new SelectListItemV1()
                {
                    Text = "Please select form"
                });

                form.FormList = formList;

                return View(form);
            }
        }

        [HttpGet]
        public IActionResult AddDictionary()
        {
            ViewData["Message"] = "Add new dictionary";

            AddDictionaryForm form = new AddDictionaryForm();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                List<SelectListItemV1> categoryList = dbConnection.Query<SelectListItemV1>(
                    @"SELECT Id AS Value, [Description] + ' - ' + Name AS Text, [Description]
                    FROM Category
                    WHERE IsActive = 1
                    ORDER BY [Description], SortOrderId"
                ).ToList();

                categoryList.Insert(0, new SelectListItemV1()
                {
                    Value = 0,
                    Text = "Please select category"
                });

                form.CategoryList = categoryList;

                return View(form);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddDictionary(AddDictionaryForm form)
        {
            ViewData["Message"] = "Add new dictionary";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);

            if (ModelState.IsValid)
            {
                using (IDbConnection dbConnection = conn)
                {
                    dbConnection.Open();
                    using (var trans = dbConnection.BeginTransaction())
                    {
                        try
                        {
                            dbConnection.Execute(@"INSERT INTO Dictionary (CategoryId, Name, Description, IsActive, SortOrderId, [CreatedBy], [CreatedDate], [Timestamp])
                                Values(@categoryId, @name, @description, @isActive, @sortOrderId, @createdBy, @createdDate, @timestamp)", new
                            {
                                @categoryId = form.SelectedCategoryId,
                                @name = form.Name,
                                @description = form.Description,
                                @isActive = 1,
                                @sortOrderId = form.SortOrderId,
                                @createdBy = this.User.Identity.Name,
                                @createdDate = DateTimeOffset.Now,
                                @timestamp = DateTimeOffset.Now
                            }, trans);

                            trans.Commit();
                            conn.Close();
                            return RedirectToAction("Index", "Home");
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            conn.Close();
                            return RedirectToAction("Error", ex);
                        }
                    }
                }
            }
            using (IDbConnection dbConnection = conn)
            {
                List<SelectListItemV1> categoryList = dbConnection.Query<SelectListItemV1>(
                    @"SELECT Id AS Value, [Description] + ' - ' + Name AS Text, [Description]
                    FROM Category
                    WHERE IsActive = 1
                    ORDER BY [Description], SortOrderId"
                ).ToList();

                categoryList.Insert(0, new SelectListItemV1()
                {
                    Value = 0,
                    Text = "Please select category"
                });

                form.CategoryList = categoryList;

                return View(form);
            }
        }

        [HttpGet]
        public IActionResult AddRegionalManager()
        {
            ViewData["Message"] = "Add new regional manager";

            AddRegionalManagerForm form = new AddRegionalManagerForm();
            SqlConnection unifiedConn = new SqlConnection();
            unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
            using (IDbConnection dbConnection = unifiedConn)
            {
                dbConnection.Open();
                List<SelectListItemV2> regionList = dbConnection.Query<SelectListItemV2>(
                    @"SELECT DISTINCT Region AS DisplayMember, region AS ValueMember
                    FROM DARProjects"
                ).ToList();

                regionList.Insert(0, new SelectListItemV2
                {
                    ValueMember = "",
                    DisplayMember = "Please select region"
                });

                form.RegionList = regionList;

                dbConnection.Close();

                return View(form);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddRegionalManager(AddRegionalManagerForm form)
        {
            ViewData["Message"] = "Add new dictionary";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);

            if (ModelState.IsValid)
            {
                using (IDbConnection dbConnection = conn)
                {
                    dbConnection.Open();
                    var region = dbConnection.Query<string>(
                        @"SELECT Region
                        FROM RegionalManager
                        WHERE Email = @email AND Region = @region", new { @email = form.Email, @region = form.SelectedRegionId  }
                    ).FirstOrDefault();

                    if (string.IsNullOrEmpty(region))
                    {
                        form.IsManagerExist = false;

                        using (var trans = dbConnection.BeginTransaction())
                        {
                            try
                            {
                                dbConnection.Execute(@"INSERT INTO RegionalManager (FirstName, LastName, Region, Email, IsActive, [CreatedBy], [CreatedDate], [Timestamp])
                                Values(@firstName, @lastName, @region, @email, @isActive, @createdBy, @createdDate, @timestamp)", new
                                {
                                    @firstName = form.FirstName,
                                    @lastName = form.LastName,
                                    @region = form.SelectedRegionId,
                                    @email = form.Email,
                                    @isActive = 1,
                                    @createdBy = this.User.Identity.Name,
                                    @createdDate = DateTimeOffset.Now,
                                    @timestamp = DateTimeOffset.Now
                                }, trans);

                                trans.Commit();
                                conn.Close();
                                return RedirectToAction("Index", "Home");
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                conn.Close();
                                return RedirectToAction("Error", ex);
                            }
                        }
                    }
                    else
                    {
                        form.IsManagerExist = true;
                    }
                }
            }

            SqlConnection unifiedConn = new SqlConnection();
            unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
            using (IDbConnection dbConnection = unifiedConn)
            {
                dbConnection.Open();
                List<SelectListItemV2> regionList = dbConnection.Query<SelectListItemV2>(
                    @"SELECT DISTINCT Region AS DisplayMember, region AS ValueMember
                    FROM DARProjects"
                ).ToList();

                regionList.Insert(0, new SelectListItemV2
                {
                    ValueMember = "",
                    DisplayMember = "Please select region"
                });

                form.RegionList = regionList;

                dbConnection.Close();

                return View(form);
            }
        }
    }
}