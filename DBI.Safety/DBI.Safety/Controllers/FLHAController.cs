﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dapper;
using DBI.Safety.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using System.Data.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Hosting;

namespace DBI.Safety.Controllers
{
    [Authorize]
    public class FLHAController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly IHostingEnvironment environment;
        private string defaultConnectionString { get; set; }
        private string appSafetyDb { get; set; }
        public FLHAController(IConfiguration config, IHostingEnvironment env)
        {
            this.configuration = config;
            this.environment = env;

            if (environment.EnvironmentName == "Development" || environment.EnvironmentName == "Staging")
            {
                this.defaultConnectionString = "DefaultStagingConnection";
                this.appSafetyDb = "AppSafetyTestDb";
            }
            else
            {
                this.defaultConnectionString = "DefaultConnection";
                this.appSafetyDb = "AppSafetyDb";
            }
        }

        public enum YesOrNoEnum
        {
            Yes,
            No  
        }

        public JsonResult GetTaskList(int locationId)
        {
            SqlConnection unifiedConn = new SqlConnection();
            unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
            using (IDbConnection dbConnection = unifiedConn)
            {
                dbConnection.Open();
                List<SelectListItemV2> taskList = dbConnection.Query<SelectListItemV2>
                    ("App_Sel_ValueNamePair", new
                    {
                        pairName = "DARTask_ByProject",
                        IntVar1 = 0,
                        IntVar2 = 0,
                        StrVar1 = locationId,
                        StrVar2 = ""
                    },
                    commandType: CommandType.StoredProcedure).ToList();
                dbConnection.Close();

                taskList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "Select" });
                return Json(new SelectList(taskList, "ValueMember", "DisplayMember"));
            }
        }

        public FLHAStep1Form GetFLHAForm(FLHAStep1Form activeForm = null)
        {
            Response.Cookies.Delete("step1");
            Response.Cookies.Delete("ppeInspected");
            Response.Cookies.Delete("locationFLHA");
            Response.Cookies.Delete("taskFLHA");
            Response.Cookies.Delete("work");
            Response.Cookies.Delete("taskLocation");
            Response.Cookies.Delete("musterPoint");
            Response.Cookies.Delete("permitJob");
            Response.Cookies.Delete("IsToolsAndEquipmentCompleted");
            Response.Cookies.Delete("IsWarningRibbonNeeded");
            Response.Cookies.Delete("IsWorkerWorkingAlone");
            Response.Cookies.Delete("Explain");
            Response.Cookies.Delete("FLHADataId");

            FLHAStep1Form form = new FLHAStep1Form();

            var yesOrNo = new List<YesOrNo>();
            yesOrNo.Add(new YesOrNo { IsSelected = true, Name = YesOrNoEnum.Yes.ToString() });
            yesOrNo.Add(new YesOrNo { IsSelected = false, Name = YesOrNoEnum.No.ToString() });
            form.YesOrNo = yesOrNo;

            SqlConnection unifiedConn = new SqlConnection();
            unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
            using (IDbConnection dbConnection = unifiedConn)
            {
                dbConnection.Open();
                List<SelectListItemV2> locationList = dbConnection.Query<SelectListItemV2>
                    ("App_Sel_ValueNamePair", new
                    {
                        pairName = "DARProjects_ByEmployee",
                        IntVar1 = 2,
                        IntVar2 = 0,
                        StrVar1 = this.User.Identity.Name,
                        StrVar2 = "SafetyApp"
                    },
                    commandType: CommandType.StoredProcedure).ToList();

                form.LocationList = locationList;

                if (activeForm != null)
                {
                    if (!string.IsNullOrEmpty(activeForm.SelectedLocationId))
                    {
                        form.SelectedLocationId = activeForm.SelectedLocationId;
                        form.SelectedTaskId = activeForm.SelectedTaskId;
                        List<SelectListItemV2> taskList = dbConnection.Query<SelectListItemV2>
                                ("App_Sel_ValueNamePair", new
                                {
                                    pairName = "DARTask_ByProject",
                                    IntVar1 = 0,
                                    IntVar2 = 0,
                                    StrVar1 = activeForm.SelectedLocationId,
                                    StrVar2 = ""
                                },
                                commandType: CommandType.StoredProcedure).ToList();

                        taskList.Insert(0, new SelectListItemV2()
                        {
                            ValueMember = "0",
                            DisplayMember = "Please select task"
                        });

                        form.TaskList = taskList;
                    }
                    else
                    {
                        List<SelectListItemV2> taskList = new List<SelectListItemV2>();
                        taskList.Insert(0, new SelectListItemV2()
                        {
                            ValueMember = "0",
                            DisplayMember = "Please select task"
                        });

                        form.TaskList = taskList;
                    }
                }
                else
                {
                    List<SelectListItemV2> taskList = new List<SelectListItemV2>();
                    taskList.Insert(0, new SelectListItemV2()
                    {
                        ValueMember = "0",
                        DisplayMember = "Please select task"
                    });

                    form.TaskList = taskList;
                }

                dbConnection.Close();
            }

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                form.EnvironmentalHazards = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id 
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Environmental Hazards' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                form.ErgonomicHazards = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id 
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Ergonomic Hazards' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                form.AccessAndOperationalHazards = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id 
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Access-Egress/ Operational Hazards' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                dbConnection.Close();
                return form;
            }
        }

        [HttpGet("FLHA/Step1")]
        public IActionResult Create()
        {
            ViewData["Message"] = "Step 1 - Fill your field level hazard assessment";

            return View(GetFLHAForm());
        }

        [HttpPost("FLHA/Step1")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(FLHAStep1Form form)
        {
            foreach (var item in form.AccessAndOperationalHazards)
            {
                if(item.Selected)
                {
                    form.IsHazardSelected = true;
                }
            }

            foreach (var item in form.EnvironmentalHazards)
            {
                if (item.Selected)
                {
                    form.IsHazardSelected = true;
                }
            }

            foreach (var item in form.ErgonomicHazards)
            {
                if (item.Selected)
                {
                    form.IsHazardSelected = true;
                }
            }

            if (form.IsHazardSelected != null && (bool)form.IsHazardSelected)
            {
                Response.Cookies.Delete("step1");
                Response.Cookies.Delete("ppeInspected");
                Response.Cookies.Delete("locationFLHA");
                Response.Cookies.Delete("taskFLHA");
                Response.Cookies.Delete("work");
                Response.Cookies.Delete("taskLocation");
                Response.Cookies.Delete("musterPoint");
                Response.Cookies.Delete("permitJob");
                Response.Cookies.Delete("IsToolsAndEquipmentCompleted");
                Response.Cookies.Delete("IsWarningRibbonNeeded");
                Response.Cookies.Delete("IsWorkerWorkingAlone");
                Response.Cookies.Delete("Explain");

                IDictionary<string, int> dict = new Dictionary<string, int>();
                foreach (var item in form.EnvironmentalHazards)
                {
                    if (item.Selected)
                    {
                        dict.Add(item.Name, item.Id);
                    }
                }

                foreach (var item in form.ErgonomicHazards)
                {
                    if (item.Selected)
                    {
                        dict.Add(item.Name, item.Id);
                    }
                }

                foreach (var item in form.AccessAndOperationalHazards)
                {
                    if (item.Selected)
                    {
                        dict.Add(item.Name, item.Id);
                    }
                }

                if (ModelState.IsValid)
                {
                    Response.Cookies.Append("ppeInspected", form.PPEInspected);
                    Response.Cookies.Append("locationFLHA", form.SelectedLocationId);
                    Response.Cookies.Append("taskFLHA", form.SelectedTaskId);
                    Response.Cookies.Append("work", form.Work);
                    Response.Cookies.Append("taskLocation", form.Location);
                    Response.Cookies.Append("musterPoint", form.MusterPoint == null ? "" : form.MusterPoint);
                    Response.Cookies.Append("permitJob", form.PermitJob == null ? "" : form.PermitJob);
                    Response.Cookies.Append("IsToolsAndEquipmentCompleted", form.IsToolsAndEquipmentCompleted.ToString());
                    Response.Cookies.Append("IsWarningRibbonNeeded", form.IsWarningRibbonNeeded.ToString());
                    Response.Cookies.Append("IsWorkerWorkingAlone", form.IsWorkerWorkingAlone.ToString());
                    Response.Cookies.Append("Explain", form.Explain == null ? "" : form.Explain);
                    Response.Cookies.Append("step1", JsonConvert.SerializeObject(dict));

                    return RedirectToAction("Step2");
                }

                return View(GetFLHAForm(form));
            }
            else
            {
                form.IsHazardSelected = false;
                return View(GetFLHAForm(form));
            }
        }

        public FLHAStep2Form GetFLHAStep2()
        {
            FLHAStep2Form form = new FLHAStep2Form();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                List<FLHADictionaryViewModel> severityList = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Severity' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                List<FLHADictionaryViewModel> probabilityList = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Probability' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                var severity = new List<SelectListItemV1>();
                foreach (var item in severityList)
                {
                    severity.Add(new SelectListItemV1 {
                        Value = item.Id,
                        Text = "(" + item.SortOrderId + ") " + item.Name.ToString() + ": " + item.Description,
                        Description = item.Description.ToString() });
                }

                severity.Insert(0, new SelectListItemV1()
                {
                    Value = 0,
                    Text = "Please choose severity"
                });

                var probability = new List<SelectListItemV1>();
                foreach (var item in probabilityList)
                {
                    probability.Add(new SelectListItemV1 {
                        Value = item.Id,
                        Text = "(" + item.SortOrderId + ") " + item.Name.ToString() + ": " + item.Description,
                        Description = item.Description.ToString() });
                }

                probability.Insert(0, new SelectListItemV1()
                {
                    Value = 0,
                    Text = "Please choose probability"
                });

                JObject step1Form = JObject.Parse(Request.Cookies["step1"]);
                IList<int> step1List = step1Form.Root.Select(c => (int)c).ToList();

                List<HazardForm> hazardForms = new List<HazardForm>();
                foreach (var item in step1List)
                {
                    var hazard = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id 
                    WHERE d.Id IN (@hazardList) AND d.IsActive = 1 AND c.IsActive = 1
                    ORDER BY d.SortOrderId",
                    new { @hazardList = new[] { item } }).FirstOrDefault();

                    hazardForms.Add(new HazardForm
                    {
                        HazardId = hazard.Id,
                        Name = hazard.Name,
                        Title = hazard.Category,
                        Severity = severity,
                        Probability = probability
                    });
                }

                form.HazardForms = hazardForms;
                form.SeverityInfo = severity.Where(x => x.Value != 0);
                form.ProbabilityInfo = probability.Where(x => x.Value != 0);                
                form.IsLocationEnabled = true;

                return form;
            }
        }

        [HttpGet("FLHA/Step2")]
        public IActionResult Step2()
        {
            ViewData["Message"] = "Continue fill your field level hazard assessment";            
           
            if(Request.Cookies["step1"] == null)
            {
                return RedirectToAction("Step1", "FLHA");
            }            

            return View(GetFLHAStep2());
        }        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Step2(FLHAStep2Form form)
        {
            if (ModelState.IsValid)
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);                

                if (Request.Cookies["latitude"] != null && Request.Cookies["longitude"] != null &&
                Request.Cookies["accuracy"] != null)
                {
                    using (IDbConnection dbConnection = conn)
                    {
                        dbConnection.Open();
                        List<SelectListItemV2> Priority = new List<SelectListItemV2>();

                        foreach (var item in form.HazardForms)
                        {
                            var Severity = dbConnection.Query<Int32>(
                                    @"SELECT SortOrderId
                                        FROM Dictionary
                                        WHERE Id = @id", new { @id = item.SelectedSeverity }
                                ).FirstOrDefault();

                            var Probability = dbConnection.Query<Int32>(
                                    @"SELECT SortOrderId
                                        FROM Dictionary
                                        WHERE Id = @id", new { @id = item.SelectedProbability }
                                ).FirstOrDefault();

                            Priority.Add(new SelectListItemV2
                            {
                                DisplayMember = item.HazardId.ToString(),
                                ValueMember = (Severity * Probability).ToString()
                            });
                        }

                        using (var trans = dbConnection.BeginTransaction())
                        {
                            try
                            {
                                var flhaId = 0;
                                flhaId = conn.Query<int>(@"INSERT INTO FLHAData ([ProjectLocation], [Task], [WorkInfo], [TaskLocation], [MusterPoint], [PermitJobNumber], [PPEInspected], 
                                [IsToolsEquipmentCompleted], [IsWarningRibbonNeeded], [IsWorkerWorkingAlone], [WorkerComment], Latitude, Longitude, 
                                Accuracy, [CreatedBy], [CreatedDate], [Timestamp])
                                Values(@projectLocation, @task, @workInfo, @taskLocation, @musterPoint, @permitJobNumber, @ppeInspected, @isToolsEquipmentCompleted, @isWarningRibbonNeeded, 
                                @isWorkerWorkingAlone, @workerComment, @latitude, @longitude, @accuracy, @createdBy, @createdDate, @timestamp) 
                                SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY];", new
                                {
                                    @projectLocation = Request.Cookies["locationFLHA"],
                                    @task = Request.Cookies["taskFLHA"],
                                    @workInfo = Request.Cookies["work"],
                                    @taskLocation = Request.Cookies["taskLocation"],
                                    @musterPoint = Request.Cookies["musterPoint"],
                                    @permitJobNumber = Request.Cookies["permitJob"],
                                    @ppeInspected = Request.Cookies["ppeInspected"],
                                    @isToolsEquipmentCompleted = Request.Cookies["IsToolsAndEquipmentCompleted"],
                                    @isWarningRibbonNeeded = Request.Cookies["IsWarningRibbonNeeded"],
                                    @isWorkerWorkingAlone = Request.Cookies["IsWorkerWorkingAlone"],
                                    @workerComment = Request.Cookies["Explain"],
                                    @latitude = Convert.ToDecimal(Request.Cookies["latitude"]),
                                    @longitude = Convert.ToDecimal(Request.Cookies["longitude"]),
                                    @accuracy = Convert.ToInt32(Math.Round(Convert.ToDecimal(Request.Cookies["accuracy"]), 0)),
                                    @createdBy = this.User.Identity.Name,
                                    @createdDate = DateTimeOffset.Now,
                                    @timestamp = DateTimeOffset.Now
                                }, trans).SingleOrDefault();                                

                                foreach (var item in form.HazardForms)
                                {
                                    dbConnection.Execute(@"INSERT INTO FLHAEliminateAndControlData ([FLHADataId], [HazardId], [SeverityId], 
                                    [ProbabilityId], [Priority], [CreatedBy], [CreatedDate], [Timestamp])
                                    Values(@flhaDataId, @hazardId, @severityId, @probabilityId, @priority, @createdBy, @createdDate, @timestamp)", new
                                    {
                                        @flhaDataId = flhaId,
                                        @hazardId = item.HazardId,
                                        @severityId = item.SelectedSeverity,
                                        @probabilityId = item.SelectedProbability,
                                        @priority = Int32.Parse(Priority.Where(x => x.DisplayMember == item.HazardId.ToString()).Select(s => s.ValueMember).FirstOrDefault()),
                                        @createdBy = this.User.Identity.Name,
                                        @createdDate = DateTimeOffset.Now,
                                        @timestamp = DateTimeOffset.Now
                                    }, trans);
                                }

                                trans.Commit();
                                conn.Close();

                                // Add FLHADataId to Cookie to retrive on Step3
                                Response.Cookies.Append("FLHADataId", flhaId.ToString());

                                return RedirectToAction("Step3", "FLHA");
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                conn.Close();
                                return RedirectToAction("Error", ex);
                            }
                        }
                    }
                }
                else
                {
                    form.IsLocationEnabled = false;
                    return View(GetFLHAStep2());
                }
            }

            return View(GetFLHAStep2());
        }

        [HttpGet("FLHA/Step3")]
        public IActionResult Step3()
        {
            ViewData["Message"] = "Review your field level hazard assessment plans";

            if (Request.Cookies["FLHADataId"] == null)
            {
                return RedirectToAction("Step1", "FLHA");
            }

            FLHAStep3Form form = new FLHAStep3Form();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                form.FLHASelectedHazards = dbConnection.Query<FLHASelectedHazard>(
                    @"SELECT i.Id, d.Name AS Hazard, i.Priority, i.Plans
                    FROM FLHAEliminateAndControlData i
                    INNER JOIN Dictionary d ON i.HazardId = d.Id
                    WHERE i.FLHADataId = @flhaDataId 
                    ORDER BY i.Priority ASC", new { @flhaDataId = Convert.ToInt32(Request.Cookies["FLHADataId"]) }
                ).ToList();
            }
           
            return View(form);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Step3(FLHAStep3Form form)
        {
            if (ModelState.IsValid)
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);

                using (IDbConnection dbConnection = conn)
                {
                    dbConnection.Open();
                    using (var trans = dbConnection.BeginTransaction())
                    {
                        try
                        {
                            foreach (var item in form.FLHASelectedHazards)
                            {
                                if (item.Plans != null)
                                {
                                    dbConnection.Execute(@"UPDATE FLHAEliminateAndControlData 
                                    SET Plans = @plans
                                    WHERE Id = @id", new
                                    {
                                        @plans = item.Plans,
                                        @id = item.Id
                                    }, trans);
                                }
                            }

                            trans.Commit();
                            conn.Close();

                            return RedirectToAction("Step4", "FLHA");
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            conn.Close();
                            return RedirectToAction("Error", ex);
                        }
                    }
                }
            }
            else
            {
                return View(form);
            }
        }

        public FLHAStep4Form GetFLHAStep4()
        {
            FLHAStep4Form form = new FLHAStep4Form();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                form.FLHASelectedHazards = dbConnection.Query<FLHASelectedHazard>(
                    @"SELECT i.Id, d.Name AS Hazard, i.Priority, i.Plans
                    FROM FLHAEliminateAndControlData i
                    INNER JOIN Dictionary d ON i.HazardId = d.Id
                    WHERE i.FLHADataId = @flhaDataId 
                    ORDER BY i.Priority ASC", new { @flhaDataId = Convert.ToInt32(Request.Cookies["FLHADataId"]) }
                ).ToList();

                form.FLHAReviews = dbConnection.Query<FLHAReview>(
                    @"SELECT FullName, SignatureSvg, Type, CreatedDate
                    FROM FLHAReview
                    WHERE FLHADataId = @flhaDataId", new { @flhaDataId = Convert.ToInt32(Request.Cookies["FLHADataId"]) }
                ).ToList();
            }

            return form;
        }

        [HttpGet("FLHA/Step4")]
        public IActionResult Step4()
        {
            ViewData["Message"] = "Verify your field level hazard assessment";

            if (Request.Cookies["FLHADataId"] == null)
            {
                return RedirectToAction("Step1", "FLHA");
            }

            return View(GetFLHAStep4());
        }

        [HttpPost("FLHA/Step4")]
        public void Step4(FLHAReview form)
        {
            if (Request.Cookies["FLHADataId"] != null)
            {
                var signatureImgSvg = string.Format(@"<svg width='320' height='160' xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink'> 
                   <image width='320' height='160' xlink:href='{0}' />
                </svg>", form.SignatureSvg);

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);

                using (IDbConnection dbConnection = conn)
                {
                    dbConnection.Open();
                    using (var trans = dbConnection.BeginTransaction())
                    {
                        try
                        {
                            dbConnection.Execute(@"INSERT INTO FLHAReview ([FLHADataId], [FullName], [SignatureSvg], [Type],  
                                    [CreatedBy], [CreatedDate], [Timestamp])
                                    Values(@flhaDataId, @fullName, @signatureSvg, @type, @createdBy, @createdDate, @timestamp)", new
                            {
                                @flhaDataId = Convert.ToInt32(Request.Cookies["FLHADataId"]),
                                @fullName = form.FullName,
                                @signatureSvg = signatureImgSvg,
                                @type = form.Type,
                                @createdBy = this.User.Identity.Name,
                                @createdDate = DateTimeOffset.Now,
                                @timestamp = DateTimeOffset.Now
                            }, trans);

                            trans.Commit();
                            conn.Close();
                        }
                        catch (Exception)
                        {
                            trans.Rollback();
                            conn.Close();
                        }
                    }
                }
            }
        }

        [HttpPost("FLHA/Complete")]
        [ValidateAntiForgeryToken]
        public ActionResult Complete()
        {
            Response.Cookies.Delete("step1");
            Response.Cookies.Delete("ppeInspected");
            Response.Cookies.Delete("locationFLHA");
            Response.Cookies.Delete("taskFLHA");
            Response.Cookies.Delete("work");
            Response.Cookies.Delete("taskLocation");
            Response.Cookies.Delete("musterPoint");
            Response.Cookies.Delete("permitJob");
            Response.Cookies.Delete("IsToolsAndEquipmentCompleted");
            Response.Cookies.Delete("IsWarningRibbonNeeded");
            Response.Cookies.Delete("IsWorkerWorkingAlone");
            Response.Cookies.Delete("Explain");
            Response.Cookies.Delete("FLHADataId");

            return RedirectToAction("Index", "Home");
        }

        public JsonResult GetTaskFilterList(int locationId)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                List<SelectListItemV2> taskList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT f.Task AS ValueMember, '(' + t.task_number + ')' +
                    CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS DisplayMember                    
                    FROM " + appSafetyDb + @".dbo.FLHAData f
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON f.Task = t.task_id WHERE f.ProjectLocation = @location", new { @Location = locationId }).ToList();
                dbConnection.Close();

                return Json(new SelectList(taskList, "ValueMember", "DisplayMember"));
            }
        }

        public ActionResult History()
        {
            ViewData["Message"] = "Field level hazard assessment logs";

            FLHAHistoryForm form = new FLHAHistoryForm();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();

                List<SelectListItemV2> regionList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT p.Region AS DisplayMember, p.Region AS ValueMember 
                    FROM " + appSafetyDb + @".dbo.FLHAData f 
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON f.ProjectLocation = p.project_id").ToList();
                regionList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                form.RegionList = regionList;

                List<SelectListItemV2> locationList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT '(' + p.segment1 + ')' + ' ' + p.long_name AS DisplayMember, 
                    f.ProjectLocation AS ValueMember 
                    FROM " + appSafetyDb + @".dbo.FLHAData f
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON f.ProjectLocation = p.project_id").ToList();
                locationList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                form.LocationList = locationList;

                if (!string.IsNullOrEmpty(form.SelectedLocationId))
                {
                    List<SelectListItemV2> taskList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT f.Task AS ValueMember, '(' + t.task_number + ')' +
                    CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS DisplayMember                    
                    FROM " + appSafetyDb + @".dbo.FLHAData f
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON f.Task = t.task_id WHERE f.ProjectLocation = @location", new { @Location = form.SelectedLocationId }).ToList();
                    form.TaskList = taskList;
                }
                else
                {
                    List<SelectListItemV2> taskList = new List<SelectListItemV2>();
                    taskList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                    form.TaskList = taskList;
                }

                form.FLHALogs = new List<FLHALog>();
                form.FLHALogs = dbConnection.Query<FLHALog>(@"SELECT f.Id, '(' + p.segment1 + ') ' + p.long_name AS Project, '(' + t.task_number + ')' +
                CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Activity, p.region, f.CreatedBy, f.CreatedDate
                FROM " + appSafetyDb + @".dbo.FLHAData f
                INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON f.ProjectLocation = p.project_id
                INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON f.Task = t.task_id
                WHERE IsDeleted = 0
                ORDER BY CreatedDate DESC").ToList();

                form.FLHADetails = new List<FLHADetail>();
                form.FLHADetails = dbConnection.Query<FLHADetail>(@"SELECT f.FLHADataId AS Id, hazard.Name AS Hazard, severity.Name AS Severity, 
                probability.Name AS Probability, f.Priority, f.Plans
                FROM FLHAEliminateAndControlData f
                INNER JOIN Dictionary hazard ON f.HazardId = hazard.Id
                INNER JOIN Dictionary severity ON f.SeverityId = severity.Id
                INNER JOIN Dictionary probability ON f.ProbabilityId = probability.Id
                ORDER BY [Priority] ASC").ToList();

                form.FLHAReviewDetails = new List<FLHAReviewDetail>();
                form.FLHAReviewDetails = dbConnection.Query<FLHAReviewDetail>(@"SELECT FLHADataId AS Id, FullName, Type, CreatedDate
                FROM FLHAReview
                ORDER BY CreatedDate DESC").ToList();

                return View(form);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult History(FLHAHistoryForm form)
        {
            ViewData["Message"] = "Field level hazard logs";

            if (form.To != null)
            {
                form.To = form.To.Value.AddDays(1);
            }

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();

                List<SelectListItemV2> regionList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT p.Region AS DisplayMember, p.Region AS ValueMember 
                    FROM " + appSafetyDb + @".dbo.FLHAData f 
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON f.ProjectLocation = p.project_id").ToList();
                regionList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                form.RegionList = regionList;

                List<SelectListItemV2> locationList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT '(' + p.segment1 + ')' + ' ' + p.long_name AS DisplayMember, 
                    f.ProjectLocation AS ValueMember 
                    FROM " + appSafetyDb + @".dbo.FLHAData f
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON f.ProjectLocation = p.project_id").ToList();
                locationList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                form.LocationList = locationList;

                if (!string.IsNullOrEmpty(form.SelectedLocationId))
                {
                    List<SelectListItemV2> taskList = dbConnection.Query<SelectListItemV2>(@"SELECT DISTINCT f.Task AS ValueMember, '(' + t.task_number + ')' +
                    CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS DisplayMember                    
                    FROM " + appSafetyDb + @".dbo.FLHAData f
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON f.Task = t.task_id WHERE f.ProjectLocation = @location", new { @Location = form.SelectedLocationId }).ToList();
                    taskList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                    form.TaskList = taskList;
                }
                else
                {
                    List<SelectListItemV2> taskList = new List<SelectListItemV2>();
                    taskList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "All" });
                    form.TaskList = taskList;
                }

                List<FLHALog> logList = new List<FLHALog>();
                var sql = string.Empty;
                var sqlParameters = string.Empty;

                if (form.SelectedLocationId == "0" && form.SelectedTaskId == "0" &&
                    form.SelectedRegionId == "0" && form.From == null && form.To == null)
                {
                    sql = @"SELECT f.Id, '(' + p.segment1 + ') ' + p.long_name AS Project, '(' + t.task_number + ')' +
                    CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Activity, p.region, f.CreatedBy, f.CreatedDate
                    FROM " + appSafetyDb + @".dbo.FLHAData f
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON f.ProjectLocation = p.project_id
                    INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON f.Task = t.task_id
                    WHERE IsDeleted = 0
                    ORDER BY CreatedDate DESC";

                    logList = dbConnection.Query<FLHALog>(sql).ToList();
                }
                else
                {
                    if (form.SelectedLocationId != "0")
                    {
                        sqlParameters = "f.ProjectLocation = @Location";
                    }
                    if (form.SelectedTaskId != "0")
                    {
                        if (string.IsNullOrEmpty(sqlParameters))
                        {
                            sqlParameters = "f.Task = @Task";
                        }
                        else
                        {
                            sqlParameters = sqlParameters + " AND f.Task = @Task";
                        }
                    }
                    if (form.SelectedRegionId != "0")
                    {
                        if (string.IsNullOrEmpty(sqlParameters))
                        {
                            sqlParameters = "p.Region = @Region";
                        }
                        else
                        {
                            sqlParameters = sqlParameters + " AND p.Region = @Region";
                        }
                    }
                    if (form.From != null && form.To != null)
                    {
                        if (string.IsNullOrEmpty(sqlParameters))
                        {
                            sqlParameters = "f.CreatedDate BETWEEN @from AND @to";
                        }
                        else
                        {
                            sqlParameters = sqlParameters + " AND f.CreatedDate BETWEEN @from AND @to";
                        }
                    }

                    if (string.IsNullOrEmpty(sqlParameters))
                    {
                        sql = @"SELECT f.Id, '(' + p.segment1 + ') ' + p.long_name AS Project, '(' + t.task_number + ')' +
                        CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Activity, p.region, f.CreatedBy, f.CreatedDate
                        FROM " + appSafetyDb + @".dbo.FLHAData f
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON f.ProjectLocation = p.project_id
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON f.Task = t.task_id
                        WHERE IsDeleted = 0
                        ORDER BY CreatedDate DESC";
                    }
                    else
                    {
                        sql = @"SELECT f.Id, '(' + p.segment1 + ') ' + p.long_name AS Project, '(' + t.task_number + ')' +
                        CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Activity, p.region, f.CreatedBy, f.CreatedDate
                        FROM " + appSafetyDb + @".dbo.FLHAData f
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON f.ProjectLocation = p.project_id
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON f.Task = t.task_id
                        WHERE IsDeleted = 0 AND (" + sqlParameters + ") ORDER BY CreatedDate DESC";
                    }

                    logList = dbConnection.Query<FLHALog>(sql, new
                    {
                        @Location = form.SelectedLocationId,
                        @Task = form.SelectedTaskId,
                        @Region = form.SelectedRegionId,
                        @from = (form.From == null) ? DateTimeOffset.Now : form.From,
                        @to = (form.To == null) ? DateTimeOffset.Now : form.To,
                        @user = this.User.Identity.Name,
                        @userEmail = this.User.Identity.Name + "@dbiservices.com"
                    }).ToList();
                }

                form.FLHALogs = new List<FLHALog>();
                form.FLHADetails = new List<FLHADetail>();
                foreach (var item in logList)
                {
                    form.FLHALogs.Add(new FLHALog
                    {
                        Id = item.Id,
                        CreatedBy = item.CreatedBy,
                        Project = item.Project,
                        CreatedDate = item.CreatedDate,
                        Region = item.Region,
                        Activity = item.Activity
                    });
                }

                form.FLHADetails = new List<FLHADetail>();
                form.FLHADetails = dbConnection.Query<FLHADetail>(@"SELECT f.FLHADataId AS Id, hazard.Name AS Hazard, severity.Name AS Severity, 
                probability.Name AS Probability, f.Priority, f.Plans
                FROM FLHAEliminateAndControlData f
                INNER JOIN Dictionary hazard ON f.HazardId = hazard.Id
                INNER JOIN Dictionary severity ON f.SeverityId = severity.Id
                INNER JOIN Dictionary probability ON f.ProbabilityId = probability.Id
                ORDER BY [Priority] ASC").ToList();

                form.FLHAReviewDetails = new List<FLHAReviewDetail>();
                form.FLHAReviewDetails = dbConnection.Query<FLHAReviewDetail>(@"SELECT FLHADataId AS Id, FullName, Type, CreatedDate
                    FROM FLHAReview
                    ORDER BY CreatedDate DESC").ToList();

                return View(form);
            }
        }

        [HttpPost("FLHA/Sign")]
        public void Sign(FLHALogSign form)
        {
            var signatureImgSvg = string.Format(@"<svg width='320' height='160' 
                    xmlns = 'http://www.w3.org/2000/svg'
                    xmlns: xlink = 'http://www.w3.org/1999/xlink'> 
                <image width='320' height='160' xlink: href = '{0}' />
            </svg>", form.SignatureSvg);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);

            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                using (var trans = dbConnection.BeginTransaction())
                {
                    try
                    {
                        dbConnection.Execute(@"INSERT INTO FLHAReview ([FLHADataId], [FullName], [SignatureSvg], [Type],  
                                [CreatedBy], [CreatedDate], [Timestamp])
                                Values(@flhaDataId, @fullName, @signatureSvg, @type, @createdBy, @createdDate, @timestamp)", new
                        {
                            @flhaDataId = form.FLHADataId,
                            @fullName = form.FullName,
                            @signatureSvg = signatureImgSvg,
                            @type = "Crew Member",
                            @createdBy = this.User.Identity.Name,
                            @createdDate = DateTimeOffset.Now,
                            @timestamp = DateTimeOffset.Now
                        }, trans);

                        trans.Commit();
                        conn.Close();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        conn.Close();
                    }
                }
            }            
        }

        [HttpGet]
        [Route("FLHA/History/{id}")]
        public ActionResult LogCard(int id)
        {
            ViewData["Message"] = "Field level hazard assessment logs";

            FLHAViewModel flha = new FLHAViewModel();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString(defaultConnectionString);
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                var flhaData = dbConnection.Query<FLHAViewModel>(@"SELECT f.Id, '(' + p.segment1 + ') ' + p.long_name AS Location, '(' + t.task_number + ')' +
                    CASE WHEN LEN(t.task_description) > 0 THEN ' - ' +  t.task_description ELSE '' END AS Activity, 
		            f.WorkInfo, f.TaskLocation, f.MusterPoint, f.PermitJobNumber, f.PPEInspected, f.IsToolsEquipmentCompleted,
		            f.IsWarningRibbonNeeded, f.IsWorkerWorkingAlone, f.WorkerComment, f.CreatedBy, f.CreatedDate 
                    FROM FLHAData f 
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARProjects p ON f.ProjectLocation = p.project_id
                        INNER JOIN AppUnifiedCatalogDb.dbo.DARTask t ON f.Task = t.task_id
                    WHERE IsDeleted = 0 AND Id = @Id", new { @Id = id }).FirstOrDefault();
                flha = flhaData;

                flha.EnvironmentalHazards = dbConnection.Query<FLHADictionaryViewModel>("GET_EnvironmentalHazards_Print", new { hazardId = id }, 
                    commandType: CommandType.StoredProcedure).ToList();

                flha.ErgonomicHazards = dbConnection.Query<FLHADictionaryViewModel>("GET_ErgonomicHazards_Print", new { hazardId = id },
                    commandType: CommandType.StoredProcedure).ToList();

                flha.AccessAndOperationalHazards = dbConnection.Query<FLHADictionaryViewModel>("GET_EgressOperationalHazards_Print", new { hazardId = id },
                    commandType: CommandType.StoredProcedure).ToList();

                flha.SeverityPrint = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Severity' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                flha.ProbabilityPrint = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Probability' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                var hazards = dbConnection.Query<HazardViewModel>(@"SELECT Id, HazardId, SeverityId, ProbabilityId,
                    [Priority], Plans FROM FLHAEliminateAndControlData 
                    WHERE FLHADataId = @FLHADataId", new { @FLHADataId = id });
                flha.Hazards = new List<HazardViewModel>();

                flha.Reviews = dbConnection.Query<FLHAReview>(@"SELECT FLHADataId AS Id, FullName, Type, CreatedDate
                    FROM FLHAReview
                    WHERE FLHADataId = @FLHADataId
                    ORDER BY CreatedDate DESC", new { @FLHADataId = id }).ToList();

                foreach (var item in hazards)
                {
                    var hazard = dbConnection.Query<dynamic>(
                    @"SELECT d.Name, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    WHERE d.Id = @hazardId AND d.IsActive = 1 AND c.IsActive = 1",
                    new { @hazardId = item.HazardId }).FirstOrDefault();

                    var severity = dbConnection.Query<dynamic>(
                    @"SELECT d.Name, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    WHERE d.Id = @severityId AND d.IsActive = 1 AND c.IsActive = 1",
                    new { @severityId = item.SeverityId }).FirstOrDefault();

                    var probability = dbConnection.Query<dynamic>(
                    @"SELECT d.Name, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    WHERE d.Id = @probabilityId AND d.IsActive = 1 AND c.IsActive = 1",
                    new { @probabilityId = item.ProbabilityId }).FirstOrDefault();

                    flha.Hazards.Add(new HazardViewModel
                    {
                        Id = item.Id,
                        HazardSortOrderId = hazard.SortOrderId,
                        Name = hazard.Name,
                        Title = hazard.Category,
                        Severity = severity.Name,
                        SeveritySortOrderId = severity.SortOrderId,
                        SeverityDesc = severity.Category,
                        Probability = probability.Name,
                        ProbabilitySortOrderId = probability.SortOrderId,
                        ProbabilityDesc = probability.Category,
                        Priority = item.Priority,
                        Plans = item.Plans
                    });
                }

                return View(flha);
            }
        }
    }
}