﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Dapper;
using DBI.Safety.Helper;
using DBI.Safety.Models;
using DBI.Safety.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DBI.Safety.Controllers
{    
    public class AccountController : Controller
    {
        private readonly IConfiguration configuration;
        public AccountController(IConfiguration config)
        {
            this.configuration = config;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Account/LogIn")]
        public IActionResult LogIn(string returnUrl = null)
        {
            ViewData["Message"] = "Welcome to Safety forms";
            TempData["returnUrl"] = returnUrl;

            return View();
        }        

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LogInViewModel form)
        {
            if (ModelState.IsValid)
            {
                AccountService account = new AccountService(configuration);
                if(await account.SignIn(HttpContext, form))
                {
                    if(TempData["returnUrl"] != null)
                    {
                        if (Url.IsLocalUrl(TempData["returnUrl"].ToString()))
                        {
                            return Redirect(TempData["returnUrl"].ToString());
                        }
                    }

                    return RedirectToAction("Index", "Home");
                }
            }

            return View(form);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Logout()
        {
            AccountService account = new AccountService(configuration);
            account.SignOut(HttpContext);
            return RedirectToAction("LogIn", "Account", null);
        }
        
        [HttpGet]
        [AllowAnonymous]
        [Route("Account/AccessDenied")]
        public IActionResult AccessDenied()
        {
            ViewData["Message"] = "You don't have the permission.";

            return View();
        }
    }
}