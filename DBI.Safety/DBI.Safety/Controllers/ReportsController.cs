﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using DBI.Safety.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Dapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Web;
using Microsoft.AspNetCore.Authorization;

namespace DBI.Safety.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly IHostingEnvironment environment;
        private string defaultConnectionString { get; set; }
        private string appSafetyDb { get; set; }
        public ReportsController(IConfiguration config, IHostingEnvironment env)
        {
            this.configuration = config;
            this.environment = env;

            if (environment.EnvironmentName == "Development" || environment.EnvironmentName == "Staging")
            {
                this.defaultConnectionString = "DefaultStagingConnection";
                this.appSafetyDb = "AppSafetyTestDb";
            }
            else
            {
                this.defaultConnectionString = "DefaultConnection";
                this.appSafetyDb = "AppSafetyDb";
            }
        }

        public JsonResult GetRegionList(string companyId)
        {
            SqlConnection unifiedConn = new SqlConnection();
            unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
            using (IDbConnection dbConnection = unifiedConn)
            {
                dbConnection.Open();
                List<SelectListItemV2> regionList = dbConnection.Query<SelectListItemV2>
                    ("App_Sel_ValueNamePair", new
                    {
                        pairName = "DARProjects_SafetyReport_Region_UserCompany",
                        IntVar1 = 0,
                        IntVar2 = 0,
                        StrVar1 = User.Identity.Name,
                        StrVar2 = companyId
                    },
                    commandType: CommandType.StoredProcedure).ToList();
                dbConnection.Close();

                regionList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "Select" });
                return Json(new SelectList(regionList, "ValueMember", "DisplayMember"));
            }
        }

        public JsonResult GetProjectList(string regionId)
        {
            SqlConnection unifiedConn = new SqlConnection();
            unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
            using (IDbConnection dbConnection = unifiedConn)
            {
                dbConnection.Open();
                List<SelectListItemV2> projectList = dbConnection.Query<SelectListItemV2>
                    ("App_Sel_ValueNamePair", new
                    {
                        pairName = "DARProjects_SafetyReport_Location_UserRegion",
                        IntVar1 = 0,
                        IntVar2 = 0,
                        StrVar1 = User.Identity.Name,
                        StrVar2 = regionId
                    },
                    commandType: CommandType.StoredProcedure).ToList();
                dbConnection.Close();

                projectList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "Select" });
                return Json(new SelectList(projectList, "DisplayMember", "DisplayMember"));
            }
        }

        public SocTotalNumberForm GetSocTotalNumber()
        {
            SocTotalNumberForm form = new SocTotalNumberForm();

            List<SelectListItemV2> regionList = new List<SelectListItemV2>();
            regionList.Insert(0, new SelectListItemV2()
            {
                ValueMember = "0",
                DisplayMember = "Select"
            });
            form.RegionList = regionList;
            
            List<SelectListItemV2> projectList = new List<SelectListItemV2>();
            projectList.Insert(0, new SelectListItemV2()
            {
                ValueMember = "0",
                DisplayMember = "Select"
            });
            form.ProjectList = projectList;            

            SqlConnection unifiedConn = new SqlConnection();
            unifiedConn.ConnectionString = configuration.GetConnectionString("UnifiedCatalogConnection");
            using (IDbConnection dbConnection = unifiedConn)
            {
                dbConnection.Open();
                List<SelectListItemV2> companyList = dbConnection.Query<SelectListItemV2>
                    ("App_Sel_ValueNamePair", new
                    {
                        pairName = "DARProjects_SafetyReport_Bu_User",
                        IntVar1 = 0,
                        IntVar2 = 0,
                        StrVar1 = User.Identity.Name,
                        StrVar2 = ""
                    },
                    commandType: CommandType.StoredProcedure).ToList();

                companyList.Insert(0, new SelectListItemV2 { ValueMember = "0", DisplayMember = "Select" });
                form.CompanyList = companyList;
                dbConnection.Close();
            }

            return form;
        }

        [HttpGet("Reports/SOCReports")]
        public IActionResult SocTotalNumber()
        {
            ViewData["Message"] = "Select all the fields to generate report";

            return View(GetSocTotalNumber());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SocCounts(SocTotalNumberForm form)
        {
            ViewData["Message"] = "Select all the fields to generate report";

            if (ModelState.IsValid)
            {
                if (form.SelectedCompanyId == "0")
                {
                    form.SelectedCompanyId = "";
                }

                if (form.SelectedRegionId == "0")
                {
                    form.SelectedRegionId = "";
                }

                if (form.SelectedProjectId == "0" || form.SelectedProjectId == "Select")
                {
                    form.SelectedProjectId = "";
                }

                if (form.From != null && form.To != null)
                {                    
                    return Redirect("http://s0077.dbiservices.com/ReportServer?/SafetyApp/SOCTotalNumber+&ProjectId=" + 
                        HttpUtility.UrlEncode(form.SelectedProjectId) + "+&Company=" + HttpUtility.UrlEncode(form.SelectedCompanyId) + 
                        "+&Region=" + HttpUtility.UrlEncode(form.SelectedRegionId) + "+&From=" + HttpUtility.UrlEncode(Convert.ToDateTime(form.From.ToString()).ToShortDateString() + " 12:00 am") + 
                        "+&To=" + HttpUtility.UrlEncode(Convert.ToDateTime(form.To.ToString()).ToShortDateString() + " 11:59 pm") +
                        "+&User=" + HttpUtility.UrlEncode(User.Identity.Name) + "&rc:Parameters=Off");
                }
                else
                {
                    return Redirect("http://s0077.dbiservices.com/ReportServer?/SafetyApp/SOCTotalNumber+&ProjectId=" + 
                        HttpUtility.UrlEncode(form.SelectedProjectId) + "+&Company=" + HttpUtility.UrlEncode(form.SelectedCompanyId) +
                        "+&Region=" + HttpUtility.UrlEncode(form.SelectedRegionId) +
                        "+&User=" + HttpUtility.UrlEncode(User.Identity.Name) + "&rc:Parameters=Off");
                }                
            }

            return View(form);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SocRisk(SocTotalNumberForm form)
        {
            ViewData["Message"] = "Select all the fields to generate report";

            if (ModelState.IsValid)
            {
                if (form.SelectedCompanyId == "0")
                {
                    form.SelectedCompanyId = "";
                }

                if (form.SelectedRegionId == "0")
                {
                    form.SelectedRegionId = "";
                }

                if (form.SelectedProjectId == "0" || form.SelectedProjectId == "Select")
                {
                    form.SelectedProjectId = "";
                }

                if (form.From != null && form.To != null)
                {
                    return Redirect("http://s0077.dbiservices.com/ReportServer?/SafetyApp/SOCRisk+&ProjectId=" +
                        HttpUtility.UrlEncode(form.SelectedProjectId) + "+&Company=" + HttpUtility.UrlEncode(form.SelectedCompanyId) +
                        "+&Region=" + HttpUtility.UrlEncode(form.SelectedRegionId) + "+&From=" + HttpUtility.UrlEncode(Convert.ToDateTime(form.From.ToString()).ToShortDateString() + " 12:00 am") +
                        "+&To=" + HttpUtility.UrlEncode(Convert.ToDateTime(form.To.ToString()).ToShortDateString() + " 11:59 pm") +
                        "+&User=" + HttpUtility.UrlEncode(User.Identity.Name) + "&rc:Parameters=Off");
                }
                else
                {
                    return Redirect("http://s0077.dbiservices.com/ReportServer?/SafetyApp/SOCRisk+&ProjectId=" +
                        HttpUtility.UrlEncode(form.SelectedProjectId) + "+&Company=" + HttpUtility.UrlEncode(form.SelectedCompanyId) +
                        "+&Region=" + HttpUtility.UrlEncode(form.SelectedRegionId) +
                        "+&User=" + HttpUtility.UrlEncode(User.Identity.Name) + "&rc:Parameters=Off");
                }
            }

            return View(form);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SocDetail(SocTotalNumberForm form)
        {
            ViewData["Message"] = "Select all the fields to generate report";

            if (ModelState.IsValid)
            {
                if (form.SelectedCompanyId == "0")
                {
                    form.SelectedCompanyId = "";
                }

                if (form.SelectedRegionId == "0")
                {
                    form.SelectedRegionId = "";
                }

                if (form.SelectedProjectId == "0" || form.SelectedProjectId == "Select")
                {
                    form.SelectedProjectId = "";
                }

                if (form.From != null && form.To != null)
                {
                    return Redirect("http://s0077.dbiservices.com/ReportServer?/SafetyApp/SOCDetail+&ProjectId=" +
                        HttpUtility.UrlEncode(form.SelectedProjectId) + "+&Company=" + HttpUtility.UrlEncode(form.SelectedCompanyId) +
                        "+&Region=" + HttpUtility.UrlEncode(form.SelectedRegionId) + "+&From=" + HttpUtility.UrlEncode(Convert.ToDateTime(form.From.ToString()).ToShortDateString() + " 12:00 am") +
                        "+&To=" + HttpUtility.UrlEncode(Convert.ToDateTime(form.To.ToString()).ToShortDateString() + " 11:59 pm") +
                        "+&User=" + HttpUtility.UrlEncode(User.Identity.Name) + "&rc:Parameters=Off");
                }
                else
                {
                    return Redirect("http://s0077.dbiservices.com/ReportServer?/SafetyApp/SOCDetail+&ProjectId=" +
                        HttpUtility.UrlEncode(form.SelectedProjectId) + "+&Company=" + HttpUtility.UrlEncode(form.SelectedCompanyId) +
                        "+&Region=" + HttpUtility.UrlEncode(form.SelectedRegionId) +
                        "+&User=" + HttpUtility.UrlEncode(User.Identity.Name) + "&rc:Parameters=Off");
                }
            }

            return View(form);
        }
    }
}