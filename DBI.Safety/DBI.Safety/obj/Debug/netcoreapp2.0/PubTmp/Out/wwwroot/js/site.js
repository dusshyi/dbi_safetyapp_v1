﻿$(function () {
    $("input:radio[name='Questions[7].SelectedAnswer']").change(function () {
        var _val = $(this).val();
        if (_val === '68') {
            $('#socSpecialQuestionModal').modal('show');
        }
    });    

    $("input:button[name='workzone_no']").on('click', function () {
        $('#socSpecialQuestionConfirmModal').modal('show');
    });

    $("input:button[name='workzone_no_confirm']").on('click', function () {
        $("input:radio[name='Questions[7].SelectedAnswer'][id='67']").prop("checked", true);
        $('#8_67').addClass("active");
        $('#8_68').removeClass("active");
    });

    $(document).ready(function () {
        $("#Location").change(function () {            
            $.getJSON("/SOC/GetTaskList", { locationId: $("#Location").val() }, function (data) {
                var items = '';
                $("#Task").empty();
                $.each(data, function (i, task) {
                    items += "<option value='" + task.value + "'>" + task.text + "</option>";
                });
                $('#Task').html(items);
            });
        });
    });

    $(document).ready(function () {
        $("#LocationHistory").change(function () {
            $.getJSON("/SOC/GetTaskFilterList", { locationId: $("#LocationHistory").val() }, function (data) {
                var items = '';
                $("#TaskHistory").empty();
                $.each(data, function (i, task) {
                    items += "<option value='" + task.value + "'>" + task.text + "</option>";
                });
                $('#TaskHistory').html(items);
            });
        });
    });    

    $(document).ready(function () {
        $("#FlhaLocationHistory").change(function () {
            $.getJSON("/FLHA/GetTaskFilterList", { locationId: $("#FlhaLocationHistory").val() }, function (data) {
                var items = '';
                $("#FlhaTaskHistory").empty();
                $.each(data, function (i, task) {
                    items += "<option value='" + task.value + "'>" + task.text + "</option>";
                });
                $('#FlhaTaskHistory').html(items);
            });
        });
    });

    $(document).ready(function () {
        $("#Company").change(function () {
            $.getJSON("/Reports/GetRegionList", { companyId: $("#Company").val() }, function (data) {
                var items = '';
                $("#SOCReportRegion").empty();
                $.each(data, function (i, task) {
                    items += "<option value='" + task.value + "'>" + task.text + "</option>";
                });
                $('#SOCReportRegion').html(items);
            });
        });
    });

    $(document).ready(function () {
        $("#SOCReportRegion").change(function () {
            $.getJSON("/Reports/GetProjectList", { regionId: $("#SOCReportRegion").val() }, function (data) {
                var items = '';
                $("#SOCReportLocation").empty();
                $.each(data, function (i, task) {
                    items += "<option value='" + task.value + "'>" + task.text + "</option>";
                });
                $('#SOCReportLocation').html(items);
            });
        });
    });

    $("input:button[name='add_signature']").on('click', function () {
        $("#signatureHeader").text($("#vFullName").val());
        $('#flhaSignatureModal').modal('show');
    });

    $("input:button[name='sign']").on('click', function () {
        $("#signatureHeader").text($("#vLogFullName").val());
        $('#flhaLogSignatureModal').modal('show');
    });

    /* Signature Functions */
    var canvas = document.getElementById('signature-pad');       

    var signaturePad = "";

    if (canvas !== null) {
        signaturePad = new SignaturePad(canvas, {
            backgroundColor: 'rgb(255, 255, 255)',
            throttle: 'throttle'
        }); 
    }

    if (document.getElementById('save-svg') !== null) {
        document.getElementById('save-svg').addEventListener('click', function () {
            if (signaturePad.isEmpty()) {
                return alert("Please provide a signature first.");
            }                 

            var data = signaturePad.toDataURL('image/svg+xml');     
            
            // detect IE8 and above, and edge
            if (document.documentMode || /Edge/.test(navigator.userAgent)) {      
                //var form = {
                //    FullName: $("#vFullName").val(),
                //    SignatureSvg: data,
                //    Type: $("#hdnType").val()
                //};

                //var s = new XMLSerializer();
                // var encodedData = window.btoa(s.serializeToString(data));

                //$.ajax({
                //    type: "GET",
                //    url: "/FLHA/AddSign",
                //    dataType: "json",
                //    contentType: "application/json; charset=utf-8",
                //    data: {
                //        FullName: $("#vFullName").val(),
                //        //SignatureSvg: data,
                //        Type: $("#hdnType").val()
                //    }
                //}).done();    

                $.ajax({
                    type: "POST",
                    url: "/FLHA/Step4",
                    data: {
                        FullName: $("#vFullName").val(),
                        SignatureSvg: data,
                        Type: $("#hdnType").val()
                    }
                }).done();

                setTimeout(window.location.reload(), 1000);
            } else {                 
                $.ajax({
                    type: "POST",
                    url: "/FLHA/Step4",
                    data: {
                        FullName: $("#vFullName").val(),
                        SignatureSvg: data,
                        Type: $("#hdnType").val()
                    }
                }).done();

                setTimeout(location.reload.bind(location), 500);
            } 
        });
    }        

    if (document.getElementById('clear') !== null) {
        document.getElementById('clear').addEventListener('click', function () {
            signaturePad.clear();
        });
    }

    if (document.getElementById('save-logsvg') !== null) {
        document.getElementById('save-logsvg').addEventListener('click', function () {
            if ($("#vLogFullName").val() === "" || $("#vLogFullName").val() === undefined) {
                return alert("Please enter you full name.");
            }

            if (signaturePad.isEmpty()) {
                return alert("Please provide a signature.");
            }

            var data = signaturePad.toDataURL('image/svg+xml');            
            $.ajax({
                type: "POST",
                url: "/FLHA/Sign",
                cache: false,
                crossDomain: false,
                data: {
                    FullName: $("#vLogFullName").val(),
                    SignatureSvg: data,
                    FLHADataId: $("#hdnFlhaDataId").val()
                }
            }).done();            

            // detect IE8 and above, and edge
            if (document.documentMode || /Edge/.test(navigator.userAgent)) {
                
                setTimeout(window.location.reload(), 500);
            } else {
                setTimeout(location.reload.bind(location), 500);
            }
        });
    }

    if (document.getElementById('logclear') !== null) {
        document.getElementById('logclear').addEventListener('click', function () {
            signaturePad.clear();
            $("#vLogFullName").val('');
        });
    }    
});
