﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class SocTotalNumberForm
    {
        [DisplayName("Location/Branch")]
        //[Required]
        //[Range(1, int.MaxValue, ErrorMessage = "Please select your project")]
        public string SelectedProjectId { get; set; }
        public IEnumerable<SelectListItemV2> ProjectList { get; set; }

        [DisplayName("BU")]
        //[Required]
        //[Range(1, int.MaxValue, ErrorMessage = "Please select your company")]
        public string SelectedCompanyId { get; set; }
        public IEnumerable<SelectListItemV2> CompanyList { get; set; }

        [DisplayName("Region")]
        //[Required]
        //[Range(1, int.MaxValue, ErrorMessage = "Please select your region")]
        public string SelectedRegionId { get; set; }
        public IEnumerable<SelectListItemV2> RegionList { get; set; }

        [DataType(DataType.Date)]
        public DateTimeOffset? From { get; set; }

        [DataType(DataType.Date)]
        public DateTimeOffset? To { get; set; }
    }
}
