﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class FLHAStep1Form
    {
        [DisplayName("Location")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select your location")]
        public string SelectedLocationId { get; set; }
        public IEnumerable<SelectListItemV2> LocationList { get; set; }
        public SelectListItemV2 SelectedLocation { get; set; }

        [DisplayName("Task/Activity observed")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select your task")]
        public string SelectedTaskId { get; set; }
        public IEnumerable<SelectListItemV2> TaskList { get; set; }

        [DisplayName("Work to be done")]
        [Required]
        public string Work { get; set; }
        [DisplayName("Task Location")]
        [Required]
        public string Location { get; set; }
        [DisplayName("Muster Point")]
        public string MusterPoint { get; set; }
        [DisplayName("Permit Job")]
        public string PermitJob { get; set; }
        [DisplayName("PPE Inspected")]
        [Required]
        public string PPEInspected { get; set; }
        [DisplayName("Has a pre-use inspection of tools/equipment been completed?")]
        public bool IsToolsAndEquipmentCompleted { get; set; }
        [DisplayName("Warning ribbon needed?")]
        public bool IsWarningRibbonNeeded { get; set; }
        [DisplayName("Is the worker working alone?")]
        public bool IsWorkerWorkingAlone { get; set; }
        public string Explain { get; set; }
        public IEnumerable<YesOrNo> YesOrNo { get; set; }
        public List<FLHADictionaryViewModel> EnvironmentalHazards { get; set; }
        public List<FLHADictionaryViewModel> ErgonomicHazards { get; set; }
        public List<FLHADictionaryViewModel> AccessAndOperationalHazards { get; set; }
        public bool? IsHazardSelected { get; set; }
    }

    public class YesOrNo
    {
        public bool IsSelected { get; set; }
        public string Name { get; set; }
    }
}
