﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class FLHASelectedHazard    {
        public int Id { get; set; }
        public string Hazard { get; set; }
        public int Priority { get; set; }
        [Required]
        public string Plans { get; set; }
    }
}
