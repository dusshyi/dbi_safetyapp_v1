﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class HazardForm
    {
        public int HazardId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }        
        public string Priority { get; set; }
        public string Plans { get; set; }       

        [DisplayName("Severity")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select severity")]
        public int SelectedSeverity { get; set; }
        public IEnumerable<SelectListItemV1> Severity { get; set; }        

        [DisplayName("Probability")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select probability")]
        public int SelectedProbability { get; set; }
        public IEnumerable<SelectListItemV1> Probability { get; set; }        
    }    
}
