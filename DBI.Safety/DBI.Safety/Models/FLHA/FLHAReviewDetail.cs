﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class FLHAReviewDetail
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Type { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
    }
}
