﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class FLHALogSign
    {
        public int FLHADataId { get; set; }
        public string FullName { get; set; }
        public string SignatureSvg { get; set; }
    }
}
