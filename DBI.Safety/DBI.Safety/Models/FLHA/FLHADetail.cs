﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class FLHADetail
    {
        public int Id { get; set; }
        public string Hazard { get; set; }
        public string Severity { get; set; }
        public string Probability { get; set; }
        public string Priority { get; set; }
        public string Plans { get; set; }
    }
}
