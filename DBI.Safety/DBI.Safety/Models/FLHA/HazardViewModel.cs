﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class HazardViewModel
    {
        public int Id { get; set; }
        public int HazardId { get; set; }
        public int HazardSortOrderId { get; set; }
        public int SeverityId { get; set; }
        public int ProbabilityId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Severity { get; set; }
        public int SeveritySortOrderId { get; set; }
        public string SeverityDesc { get; set; }
        public string Probability { get; set; }
        public int ProbabilitySortOrderId { get; set; }
        public string ProbabilityDesc { get; set; }
        public string Priority { get; set; }
        public string Plans { get; set; }
    }
}
