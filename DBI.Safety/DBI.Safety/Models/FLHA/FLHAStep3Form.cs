﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class FLHAStep3Form
    {        
        public List<FLHASelectedHazard> FLHASelectedHazards { get; set; }
    }
}
