﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class FLHAStep2Form
    {
        public List<HazardForm> HazardForms { get; set; }                        
        public IEnumerable<SelectListItemV1> SeverityInfo { get; set; }
        public IEnumerable<SelectListItemV1> ProbabilityInfo { get; set; }
        public bool IsLocationEnabled { get; set; }
    }    
}
