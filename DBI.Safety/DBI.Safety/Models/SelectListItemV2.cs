﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class SelectListItemV2
    {
        public string DisplayMember { get; set; }
        public string ValueMember { get; set; }
    }
}
