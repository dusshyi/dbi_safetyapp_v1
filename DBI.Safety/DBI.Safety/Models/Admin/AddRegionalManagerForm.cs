﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DBI.Safety.Models
{
    public class AddRegionalManagerForm
    {
        [DisplayName("First Name")]
        [Required]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        [Required]
        public string LastName { get; set; }

        [DisplayName("Region")]
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 1, ErrorMessage = "Please select your region")]
        public string SelectedRegionId { get; set; }
        public IEnumerable<SelectListItemV2> RegionList { get; set; }

        [Required]
        [RegularExpression(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$", 
            ErrorMessage="Please enter valid email", MatchTimeoutInMilliseconds=250)]
        public string Email { get; set; }

        public bool IsManagerExist { get; set; }
    }
}
