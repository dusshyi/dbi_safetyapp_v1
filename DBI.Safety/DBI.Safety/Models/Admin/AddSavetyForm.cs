﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class AddSavetyForm
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
