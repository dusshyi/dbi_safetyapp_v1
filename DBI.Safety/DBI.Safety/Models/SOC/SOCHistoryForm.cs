﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class SOCHistoryForm
    {
        [DisplayName("Region")]
        public string SelectedRegionId { get; set; }
        public IEnumerable<SelectListItemV2> RegionList { get; set; }

        [DisplayName("Location")]
        public string SelectedLocationId { get; set; }
        public IEnumerable<SelectListItemV2> LocationList { get; set; }

        [DisplayName("Task/Activity observed")]
        public string SelectedTaskId { get; set; }
        public IEnumerable<SelectListItemV2> TaskList { get; set; }

        [DataType(DataType.Date)]
        public DateTimeOffset? From { get; set; }
        [DataType(DataType.Date)]
        public DateTimeOffset? To { get; set; }

        public List<SOCLog> SOCLogs { get; set; }
    }
}
