﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DBI.Safety.Models
{
    public class SOCForm
    {
        public List<QuestionsFormViewModel> Questions { get; set; }
        public Modal WorkZoneIntrusionsSpecialQuestion { get; set; }
        public Modal WorkZoneIntrusionsConfirmation { get; set; }

        [DisplayName("Other at risk observations")]
        [DataType(DataType.MultilineText)]
        public string OtherAtRiskData { get; set; }

        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }

        [DisplayName("Location")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select your location")]
        public string SelectedLocationId { get; set; }
        public IEnumerable<SelectListItemV2> LocationList { get; set; }

        [DisplayName("Task/Activity observed")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select your task")]
        public string SelectedTaskId { get; set; }
        public IEnumerable<SelectListItemV2> TaskList { get; set; }

        [DisplayName("Other positive observations")]
        public string OtherPositiveObservation { get; set; }
        public string Observer { get; set; }
        public DateTime TimeStamp { get; set; }                
        public IEnumerable<ObserverType> ObserverType { get; set; }

        [Required]
        public int? SelectedObserverType { get; set; }
        public bool IsQuestionSelected { get; set; }
        public bool IsLocationEnabled { get; set; }
        public bool IsObserverTypeSelected { get; set; }
        public string Region { get; set; }
    }

    public class ObserverType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Modal
    {
        public string Header { get; set; }
        public string Body { get; set; }
    }
}
