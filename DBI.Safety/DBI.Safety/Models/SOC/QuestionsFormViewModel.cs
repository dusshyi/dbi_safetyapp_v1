﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class QuestionsFormViewModel
    {
        public int? SelectedAnswer { get; set; }
        public IEnumerable<Answer> PossibleAnswers { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public int SortOrderId { get; set; }
    }

    public class Answer
    {
        public int Id { get; set; }
        public string Text { get; set; }        
    }
}
