﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class QuestionsViewModel
    {
        public string SelectedAnswer { get; set; }
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public int SortOrderId { get; set; }
    }
}
