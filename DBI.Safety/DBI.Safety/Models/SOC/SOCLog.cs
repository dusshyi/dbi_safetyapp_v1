﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class SOCLog
    {
        public int Id { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Location { get; set; }
        public string Region { get; set; }
        public string Task { get; set; }
    }
}
