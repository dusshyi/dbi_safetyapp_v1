﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class SelectListItemV1
    {
        public string Text { get; set; }
        public int Value { get; set; }
        public string Description { get; set; }
    }
}
